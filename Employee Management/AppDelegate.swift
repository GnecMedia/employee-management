//
//  AppDelegate.swift
//  Employee Management
//
//  Created by vidya on 12/10/2020.
//

import UIKit
import SlideMenuControllerSwift
import Firebase
import FirebaseCrashlytics
@main
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {


     var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let color2 = UIColor(rgb: 0x73AFBA)
        UINavigationBar.appearance().barTintColor = color2
        if UserDefaults.standard.bool(forKey: "isLogin") == true{
            makeRootViewController()
        }
       
        FirebaseApp.configure()
        
        Messaging.messaging().delegate = self
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()

        return true
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
      print("Firebase registration token: \(String(describing: fcmToken!))")
        UserDefaults.standard.setValue(fcmToken!, forKey: "FCMToken")
      let dataDict:[String: String] = ["token": fcmToken ?? ""]
      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }


    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("deviceToken----\(deviceToken)")
      Messaging.messaging().apnsToken = deviceToken
    }

    func sharedInstance() -> AppDelegate {
       return UIApplication.shared.delegate as! AppDelegate
    }

    func showAlertNotification(_ text: String){
        let alert = UIAlertController(title: "", message: text, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
       self.window?.rootViewController?.present(alert, animated: true, completion: nil)

    }

}

//MARK:- Other ApppDelegate Methods
extension AppDelegate {
    
    //Make Root View Controller
    func makeRootViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
         let customTabbarViewController = storyboard.instantiateViewController(withIdentifier: "CustomTabBarVC") as? CustomTabBarVC
            let leftMenuViewController = storyboard.instantiateViewController(withIdentifier: "MenuVC") as? MenuVC
        let rightMenuController = storyboard.instantiateViewController(withIdentifier: "RightMenuVC") as? RightMenuVC
       
    
        
        let navigationViewController: UINavigationController = UINavigationController(rootViewController: customTabbarViewController!)
        
        //Create Side Menu View Controller with main, left and right view controller
        let sideMenuViewController = SlideMenuController(mainViewController: navigationViewController, leftMenuViewController: leftMenuViewController!, rightMenuViewController: rightMenuController!)
        window?.rootViewController = sideMenuViewController
        window?.makeKeyAndVisible()
    }
}

