//
//  WeekOffVC.swift
//  Employee Management
//
//  Created by vidya on 02/11/2020.
//

import UIKit

class WeekOffVC: UIViewController {
    @IBOutlet weak var weekoffTbl:UITableView!
    var startDateStr:String!
    var endDateStr:String!
    var weekoff_datalist = [[String:Any]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        attendanceApiCall()
        // Do any additional setup after loading the view.
    }
    
    func attendanceApiCall(){
       // self.attendance_dataList.removeAll()
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr = baseUrl + get_report_detail
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"start_date":startDateStr!,"end_date":endDateStr!] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                let data = result["data"] as? [String:Any]
                let report_data = data!["report_data"] as! [String:Any]
                let attendance_data = report_data["weekoff_data"] as! [[String:Any]]

                for dic in attendance_data{
                    self.weekoff_datalist.append(dic)
                }
            }
            self.weekoffTbl.reloadData()
        }

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension WeekOffVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return weekoff_datalist.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeekOffCell", for: indexPath) as! WeekOffCell
        cell.selectionStyle = .none
        let item = weekoff_datalist[indexPath.row]
        cell.datelbl.text! = item["off_date"] as! String
        
         return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
}
