//
//  HalfdayLeaveVC.swift
//  Employee Management
//
//  Created by vidya on 02/11/2020.
//

import UIKit

class HalfdayLeaveVC: UIViewController {
    @IBOutlet weak var halfdayTbl:UITableView!
    var startDateStr:String!
    var endDateStr:String!
    var halfday_datalist = [[String:Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func attendanceApiCall(){
       // self.attendance_dataList.removeAll()
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr = baseUrl + get_report_detail
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"start_date":startDateStr!,"end_date":endDateStr!] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                let data = result["data"] as? [String:Any]
                let report_data = data!["report_data"] as! [String:Any]
                let attendance_data = report_data["halfday_data"] as! [[String:Any]]

                for dic in attendance_data{
                    self.halfday_datalist.append(dic)
                }
            }
            self.halfdayTbl.reloadData()
        }

    }


}
extension HalfdayLeaveVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return halfday_datalist.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllLeaveCell", for: indexPath) as! AllLeaveCell
        cell.selectionStyle = .none
        let item = halfday_datalist[indexPath.row]
        if let full_name = item["full_name"] as? String{
            cell.nameLbl.text! = full_name
        }
        if let start_date = item["start_date"] as? String{
            cell.dateLbl.text! = start_date
        }
        if let start_time = item["start_time"] as? String{
            cell.timelbl.text! = start_time
        }
        if let leave_type = item["leave_type"] as? String{
            cell.leaveTypeLbl.text! = leave_type
        }
         return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
}
