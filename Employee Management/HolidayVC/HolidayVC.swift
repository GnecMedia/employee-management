//
//  HolidayVC.swift
//  Employee Management
//
//  Created by vidya on 28/10/2020.
//

import UIKit

class HolidayVC: UIViewController {
    @IBOutlet weak var addBtn:UIButton!
    @IBOutlet weak var holidayTbl:UITableView!
    var holidaylist = [[String:Any]]()
    var menuType:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Holiday"
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
            item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
        if UserDefaults.standard.bool(forKey: "is_Admin") == true{
            self.addBtn.isHidden = false
        }else{
            self.addBtn.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    
    @objc func barButtonAction(){
        if self.menuType == "menu"{
            KAppDelegate.makeRootViewController()
        }else{
        self.navigationController?.popViewController(animated: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        if Reachability.isConnectedToNetwork() == true{
        holidayApiCall()
        }else{
            KAppDelegate.showAlertNotification(checkInternet)
        }

    }

    func holidayApiCall(){
        self.holidaylist.removeAll()
            let userid = UserDefaults.standard.object(forKey: "user_id")
            let accesToken = UserDefaults.standard.object(forKey: "access_token")
            let urlStr = baseUrl + holiday_list
            let leaveDetials = ["user_id":userid!,"access_token":accesToken!] as [String:Any]
            postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
                print("Result--\(result)")
                if succ{
                    let data = result["data"] as? [String:Any]
                    let leave = data!["holiday_data"] as! [[String:Any]]
                    for dic in leave{
                        self.holidaylist.append(dic)
                    }
                }
                self.holidayTbl.reloadData()
            }

        }
    @IBAction func addholidayBtn(sender:UIButton){
        let objvc = self.storyboard?.instantiateViewController(withIdentifier: "AddHolidayVC") as? AddHolidayVC
      self.navigationController?.pushViewController(objvc!, animated: true)

    }
    
}
extension HolidayVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return holidaylist.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HolidayCell", for: indexPath) as! HolidayCell
        let item = holidaylist[indexPath.row]
        if let festival = item["festival"] as? String{
            cell.namelbl.text! = festival
        }
        if let holiday_date = item["holiday_date"] as? String{
            cell.datelbl.text! = holiday_date
        }

        cell.selectionStyle = .none
        let holdToDelete = UILongPressGestureRecognizer(target: self, action: #selector(HolidayVC.deleteBTNAction(_:)));
        holdToDelete.minimumPressDuration = 1.00;
          cell.tag = indexPath.row
        cell.addGestureRecognizer(holdToDelete);

       return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UserDefaults.standard.bool(forKey: "is_Admin") == true{
        let item = holidaylist[indexPath.row]
        let objvc = self.storyboard?.instantiateViewController(withIdentifier: "AddHolidayVC") as? AddHolidayVC
        objvc?.updateData = item
        objvc?.upadateStatus = "update"
        self.navigationController?.pushViewController(objvc!, animated: true)
        }
    }
    
    @objc func deleteBTNAction(_ sender: UILongPressGestureRecognizer) {
        print("delete")
        let tag = sender.view!.tag
        let indexPath = IndexPath(row: tag, section: 0)
        let item = holidaylist[indexPath.row]
        let ids = item["id"] as! String
        showSimpleAlert(id: ids)

      print("ids--\(ids)")
    }
    func showSimpleAlert(id:String) {
        let alert = UIAlertController(title: "", message: "Are You Sure Want to remove this Holiday",preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
        }))
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
         print("Ok")
                                        self.deleteApiCall(id: id)
      }))
        self.present(alert, animated: true, completion: nil)
    }
    func deleteApiCall(id:String){
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr = baseUrl + delete_holiday
        let parms = ["user_id":userid!,"access_token":accesToken!,"id":id] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: parms){(succ, result) in
            print("Result--\(result)")
            if succ{
                KAppDelegate.showAlertNotification("Holiday record has been successfully deleted")
                self.holidayApiCall()
            }
        }

    }
}

