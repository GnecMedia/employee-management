//
//  CustomTabBarVC.swift
//  Employee Management
//
//  Created by vidya on 20/10/2020.
//

import UIKit
import RAMAnimatedTabBarController

class CustomTabBarVC: RAMAnimatedTabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
        super.viewWillAppear(animated)
    }


}
