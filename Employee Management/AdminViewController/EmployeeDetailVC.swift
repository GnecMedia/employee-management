//
//  EmployeeDetailVC.swift
//  Employee Management
//
//  Created by vidya on 09/11/2020.
//

import UIKit

class EmployeeDetailVC: UIViewController {
    @IBOutlet weak var namelbl:UILabel!
    @IBOutlet weak var degnignationlbl:UILabel!
    @IBOutlet weak var profileImg:UIImageView!
    var nameStr:String!
    var designationStr:String!
    var profileStr:String!
    var emp_id:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Employee Detail"
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
            item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
        self.namelbl.text! = nameStr!
        self.degnignationlbl.text! = designationStr!
        self.profileImg.layer.cornerRadius = self.profileImg.frame.height / 2
        self.profileImg.sd_setImage(with: URL(string: profileStr!))
        // Do any additional setup after loading the view.
    }
    
    @objc func barButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func allBtn_Action(sender:UIButton){
        switch sender.tag {
        case 100:
            print("Report")
            let objvc = self.storyboard?.instantiateViewController(withIdentifier: "ReportVC") as? ReportVC
          self.navigationController?.pushViewController(objvc!, animated: true)

            break
        case 101:
            print("project")
            let objvc = self.storyboard?.instantiateViewController(withIdentifier: "ProjectVC") as? ProjectVC
          self.navigationController?.pushViewController(objvc!, animated: true)

            break
        case 102:
            print("leave")
            let objvc = self.storyboard?.instantiateViewController(withIdentifier: "AllLeaveVC") as? AllLeaveVC
          self.navigationController?.pushViewController(objvc!, animated: true)

            break
        case 103:
            print("Bills")
            let objvc = self.storyboard?.instantiateViewController(withIdentifier: "AllBillsVC") as? AllBillsVC
          self.navigationController?.pushViewController(objvc!, animated: true)

            break
        case 104:
            let objvc = self.storyboard?.instantiateViewController(withIdentifier: "Admin_RegisterVC") as? Admin_RegisterVC
            objvc!.emp_id = emp_id
            objvc!.updateType = "update"
          self.navigationController?.pushViewController(objvc!, animated: true)

            print("profile info")
            break
        case 105:
            clear_imeiAPiCall()
            print("clear IMEI")
            break


        default:
            break
        }
    }
   
    func clear_imeiAPiCall(){
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr = baseUrl + clear_imei
        let imeiDetials = ["user_id":userid!,"access_token":accesToken!,"emp_id":emp_id!] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: imeiDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                KAppDelegate.showAlertNotification(result["message"] as! String)
                
            }
        }
    }
}
