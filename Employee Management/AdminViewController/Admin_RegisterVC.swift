//
//  Admin_RegisterVC.swift
//  Employee Management
//
//  Created by vidya on 09/11/2020.
//

import UIKit
import DLRadioButton

class Admin_RegisterVC: UIViewController,UITextFieldDelegate {
    fileprivate var popViewController :  DateTimePopUp!
    @IBOutlet var fullname:UILabel!
    @IBOutlet var deginationLbl:UILabel!
    @IBOutlet var profileImg:UIImageView!

    @IBOutlet var maleButton : DLRadioButton!
    @IBOutlet var maleBtn : DLRadioButton!
    @IBOutlet var femaleBtn : DLRadioButton!
    @IBOutlet var adminYesBtn : DLRadioButton!
    @IBOutlet var adminNoBtn : DLRadioButton!

    @IBOutlet var employee_idTxt : UITextField!
    @IBOutlet var emailtxt : UITextField!
    @IBOutlet var passwordtxt : UITextField!
    @IBOutlet var full_nameTxt : UITextField!
    @IBOutlet var genderTxt : UITextField!
    @IBOutlet var designationTxt : UITextField!
    @IBOutlet var departmentTxt : UITextField!
    @IBOutlet var phone_numberTxt : UITextField!
    @IBOutlet var joining_dateTxt : UITextField!
    @IBOutlet var is_adminTxt : UITextField!
    @IBOutlet var created_bytxt : UITextField!
    @IBOutlet var date_of_birthTxt : UITextField!
    @IBOutlet var blood_groupTxt : UITextField!
    @IBOutlet var addressTxt : UITextField!
    @IBOutlet var aadhaar_numbertxt : UITextField!
    @IBOutlet var accountNumber : UITextField!
    @IBOutlet var dobTxt : UITextField!
    @IBOutlet var joiningdateTxt : UITextField!

    @IBOutlet var pan_numberTxt : UITextField!
    @IBOutlet var uan_numbertxt : UITextField!
    @IBOutlet var esic_numberTxt : UITextField!
    @IBOutlet var ifsc_codetxt : UITextField!
    @IBOutlet var imageTxtx : UITextField!
    @IBOutlet var addBtn : UIButton!

     var selectprofileImg = UIImageView()
    var genderStr:String! = ""
    var adminStr:String! = ""
    var menuType:String!
    var updateType:String! = ""
    var emp_id:String! = ""
    var status:String! = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Profile"
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
            item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
        if updateType == "update"{
            self.addBtn.setTitle("Update", for: .normal)
          empolyeeIDApi()
        }
        // Do any additional setup after loading the view.
    }
    
    @objc func barButtonAction(){
        if self.menuType == "menu"{
            KAppDelegate.makeRootViewController()
        }else{
        self.navigationController?.popViewController(animated: true)
        }

    }
    func empolyeeIDApi(){
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr = baseUrl + get_employee_by_id
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"emp_id":emp_id!] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                let data = result["data"] as? [String:Any]
                let employee_data = data!["employee_data"] as! [String:Any]
                let designation = employee_data["designation"] as! String
                self.deginationLbl.text! = designation
                let name = employee_data["full_name"] as! String
                self.fullname.text! = name
                if let employee_id = employee_data["employee_id"] as? String{
                    self.employee_idTxt.text! = employee_id
                }
                if let full_name = employee_data["full_name"] as? String{
                    self.full_nameTxt.text!  = full_name
                }
                if let email = employee_data["email"] as? String{
                    self.emailtxt.text! = email
                }
                if let is_admin = employee_data["is_admin"] as? String{
                    print("vidya--\(is_admin)")
                    if is_admin == "true"{
                        for radioButton in self.adminNoBtn.otherButtons {
                            radioButton.isSelected = true;
                            self.adminStr = is_admin
                        }
                        }else{
                            for radioButton in self.adminYesBtn.otherButtons {
                                radioButton.isSelected = true;
                                self.adminStr = is_admin
                            }
                        }
                }
                if let gender = employee_data["gender"] as? String{
                    print("gender--\(gender)")
                    if gender == "Female"{
                        for radioButton in self.maleBtn.otherButtons {
                            radioButton.isSelected = true;
                            self.genderStr = gender
                        }

                    }else{
                        for radioButton in self.femaleBtn.otherButtons {
                            radioButton.isSelected = true;
                            self.genderStr = gender
                        }

                    }

                }
                if let designation = employee_data["designation"] as? String{
                    self.designationTxt.text! = designation
                }
                if let dept = employee_data["department"] as? String{
                    self.departmentTxt.text! = dept
                }
                if let password = employee_data["password"] as? String{
                    self.passwordtxt.text! = password
                }
                if let address = employee_data["address"] as? String{
                    self.addressTxt.text! = address
                }
                if let account_number = employee_data["account_number"] as? String{
                    self.accountNumber.text! = account_number
                }
                if let ifsc_code = employee_data["ifsc_code"] as? String{
                    self.ifsc_codetxt.text! = ifsc_code
                }
                if let aadhaar_number = employee_data["aadhaar_number"] as? String{
                    self.aadhaar_numbertxt.text! = aadhaar_number
                }
                if let pan_number = employee_data["pan_number"] as? String{
                    self.pan_numberTxt.text! = pan_number
                }
                if let uan_number = employee_data["uan_number"] as? String{
                    self.uan_numbertxt.text! = uan_number
                }
                if let esic_number = employee_data["esic_number"] as? String{
                    self.esic_numberTxt.text! = esic_number
                }
                if let phone_number = employee_data["phone_number"] as? String{
                    self.phone_numberTxt.text! = phone_number
                }
                if let joining_date = employee_data["joining_date"] as? String{
                    self.joining_dateTxt.text! = joining_date
                }
                if let date_of_birth = employee_data["date_of_birth"] as? String{
                    self.dobTxt.text! = date_of_birth
                }
                self.status = employee_data["status"] as? String
                self.profileImg.layer.cornerRadius = self.profileImg.frame.height / 2
                let profile_picture = employee_data["profile_picture"] as! String
                self.profileImg.sd_setImage(with: URL(string: profile_picture))

                self.selectprofileImg.image = self.profileImg.image//UIImage(named:self.profileImg.image)
            }
        }
    }
    @IBAction func selectDateBtnAction(sender:UIButton){
        switch sender.tag {
        case 100:
            print("startDate")
            let bundle = Bundle(for: DateTimePopUp.self)
            self.popViewController = DateTimePopUp(nibName: "DateTimePopUp", bundle: bundle)
          self.popViewController.delegate = self
            self.popViewController.showInView(self.view,animated: true)

            break
        case 101:
            print("endDate")
            let bundle = Bundle(for: DateTimePopUp.self)
            self.popViewController = DateTimePopUp(nibName: "DateTimePopUp", bundle: bundle)
          self.popViewController.delegate = self
            self.popViewController.dateMode = "end_Date"
            self.popViewController.showInView(self.view,animated: true)

            break
            
        default:
            break
        }
        
    }
    @IBAction func upadateBtn_Action(sender:UIButton){
        if self.employee_idTxt.text?.isEmpty != true{
            if self.full_nameTxt.text?.isEmpty != true{
                if self.emailtxt.text?.isEmpty != true{
                    if self.phone_numberTxt.text?.isEmpty != true{
                        if self.genderStr != ""{
                            if self.adminStr != ""{
                                if self.designationTxt.text?.isEmpty != true{
                                    if self.departmentTxt.text?.isEmpty != true{
                                        if self.passwordtxt.text?.isEmpty != true{
                                            createAccountApi()

                                        }else{
                                            KAppDelegate.showAlertNotification("Please Enter Password")
                                        }
                                    }else{
                                        KAppDelegate.showAlertNotification("Please Enter Department")
                                    }
                                }else{
                                    KAppDelegate.showAlertNotification("Please Enter Degination")
                                }
                            }else{
                                KAppDelegate.showAlertNotification("Please select admin")
                            }
                        }else{
                            KAppDelegate.showAlertNotification("Please select Gender")
                        }
                    }else{
                        KAppDelegate.showAlertNotification("Please enter Phone")
                    }
                }else{
                    KAppDelegate.showAlertNotification("Please Enter Email")
                }
            }else{
                KAppDelegate.showAlertNotification("Please Enter Full Name")
            }
        }else{
            KAppDelegate.showAlertNotification("Please Enter Employee Id")
        }
    }
    @IBAction func logSelectedButton(radioButton : DLRadioButton) {
        
        if (radioButton.isMultipleSelectionEnabled) {
            for button in radioButton.selectedButtons() {
                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
            }
        } else {
            print(String(format: "%@ is selectedw.\n", radioButton.selected()!.titleLabel!.text!));
            let selectmale = radioButton.selected()!.titleLabel!.text!
            self.genderStr = selectmale
        }

    }
    @IBAction func admin_BtnAction(radioButton:DLRadioButton){
        print(String(format: "%@ is selectedw.\n", radioButton.selected()!.titleLabel!.text!));
        let selectadmin = radioButton.selected()!.titleLabel!.text!
        self.adminStr = selectadmin

    }
    
    func createAccountApi(){
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let pngImage = selectprofileImg.image!.jpegData(compressionQuality: 0.5)
        let uploadedAttImg = UIImage(data: (pngImage! as NSData) as Data)
        let urlStr:String!
        if self.updateType == "update"{
            urlStr = baseUrl + update_employee
        }else{
           urlStr = baseUrl + add_employee
        }
        let addemployeeDetials = ["user_id":userid!,"access_token":accesToken!,"employee_id":self.employee_idTxt.text!,"email":self.emailtxt.text!,"password":self.passwordtxt.text!,"full_name":self.full_nameTxt.text!,"gender":genderStr!,"designation":self.designationTxt.text!,"department":self.departmentTxt.text!,"phone_number":self.phone_numberTxt.text!,"joining_date":self.joining_dateTxt.text!,"is_admin":adminStr!,"created_by":userid!,"date_of_birth":self.dobTxt.text!,"blood_group":self.blood_groupTxt.text!,"address":self.addressTxt.text!,"aadhaar_number":self.aadhaar_numbertxt.text!,"pan_number":self.pan_numberTxt.text!,"uan_number":self.uan_numbertxt.text!,"esic_number":self.esic_numberTxt.text!,"account_number":self.accountNumber.text!,"ifsc_code":self.ifsc_codetxt.text!,"emp_id":emp_id!,"status":status!] as [String:Any]
        postadminProfileData(urlStr:urlStr, parameter: addemployeeDetials, selectedevent: uploadedAttImg){(succ, result) in
            print("Result--\(result)")
            if succ{
                KAppDelegate.showAlertNotification(result["message"] as! String)
                
            }
        }
    }
}
extension Admin_RegisterVC:DatePickerPopUpViewDelegate{
    func didPresDoneActionBtn(_ controller: DateTimePopUp, isDate: String) {
        self.joining_dateTxt.text! = isDate
    }
    
    func didPresDoneTimeActionBtn(_ controller: DateTimePopUp, isTime: String) {
        print("time")

    }
    
    func didPresEndDateActionBtn(_ controller: DateTimePopUp, isendDate: String) {
        print("time")
        self.dobTxt.text! = isendDate

    }
    
    func didPresEndTimeActionBtn(_ controller: DateTimePopUp, isendTime: String) {
        print("time")
    }
    
    
}
extension Admin_RegisterVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    @IBAction func attactedBtnAction(sender:UIButton){
    let alert = UIAlertController(title: "Select Image From", message: "", preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
                    let imagePickerController=UIImagePickerController()
                    imagePickerController.delegate = self
                    imagePickerController.sourceType = .camera
                    imagePickerController.cameraCaptureMode = .photo
                    imagePickerController.cameraDevice = .front
                    self.view.window!.rootViewController?.present(imagePickerController, animated: true, completion: nil)
                }))

                alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
                    let imagePickerController=UIImagePickerController()
                    imagePickerController.delegate = self
                    imagePickerController.sourceType =  UIImagePickerController.SourceType.photoLibrary
                  self.view.window!.rootViewController?.present(imagePickerController, animated: true, completion: nil)
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                }))
                self.present(alert, animated: true, completion: {
                })
  }
     func imagePickerController(_ picker: UIImagePickerController,didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
         let image = info[.originalImage] as! UIImage
          selectprofileImg.image = image
        print(image)

         picker.dismiss(animated: true, completion: nil)

     }
     func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
         picker.dismiss(animated: true, completion: nil)
     }
    func saveEditedImageToDocumentDirectory(_ image: UIImage) -> String{
        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        // create a name for your image
        let fileURL = documentsDirectoryURL.appendingPathComponent("file.jpg")
        
        if !FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try image.jpegData(compressionQuality: 1.0)!.write(to: fileURL, options: .atomic)
                print("file saved")
                
            } catch {
                print(error)
            }
        }
        
       return "nil"
}

}

