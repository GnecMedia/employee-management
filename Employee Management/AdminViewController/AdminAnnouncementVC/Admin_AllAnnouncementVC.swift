//
//  Admin_AllAnnouncementVC.swift
//  Employee Management
//
//  Created by vidya on 10/11/2020.
//

import UIKit

class Admin_AllAnnouncementVC: UIViewController {
    var popViewController:DateTimePopUp!
    @IBOutlet weak var annocementTbl:UITableView!
    @IBOutlet weak var addBtn:UIButton!
    @IBOutlet weak var startDateBtn:UIButton!
    @IBOutlet weak var endDateBtn:UIButton!

    var announcementList = [[String:Any]]()
    var menuType:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "All Announcement"
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
            item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
        if UserDefaults.standard.bool(forKey: "is_Admin") == true{
            self.addBtn.isHidden = false
        }else{
            self.addBtn.isHidden = true
        }

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if Reachability.isConnectedToNetwork() == true{
        get_annoncement_ApiCall()
        }else{
            KAppDelegate.showAlertNotification(checkInternet)
        }
    }
    func get_annoncement_ApiCall(){
        self.announcementList.removeAll()
            let userid = UserDefaults.standard.object(forKey: "user_id")
            let accesToken = UserDefaults.standard.object(forKey: "access_token")
            let urlStr = baseUrl + get_announcement
            let leaveDetials = ["user_id":userid!,"access_token":accesToken!] as [String:Any]
            postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
                print("Result--\(result)")
                if succ{
                    let data = result["data"] as? [String:Any]
                    let annocement_data = data!["announcement_data"] as! [[String:Any]]
                    for dic in annocement_data{
                        self.announcementList.append(dic)
                    }
                }
                self.annocementTbl.reloadData()
            }
    }

    @objc func barButtonAction(){
        if self.menuType == "menu"{
            KAppDelegate.makeRootViewController()
        }else{
        self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func addAnnounceBtn_Action(sender:UIButton){
        let objvc = self.storyboard?.instantiateViewController(withIdentifier: "Admin_AddAnnounceVC") as? Admin_AddAnnounceVC
      self.navigationController?.pushViewController(objvc!, animated: true)

    }
    @IBAction func selectedDateBtnAction(sender:UIButton){
        switch sender.tag {
        case 100:
            let bundle = Bundle(for: DateTimePopUp.self)
            self.popViewController = DateTimePopUp(nibName: "DateTimePopUp", bundle: bundle)
          self.popViewController.delegate = self
            self.popViewController.showInView(self.view,animated: true)

            break
        case 101:
            let bundle = Bundle(for: DateTimePopUp.self)
            self.popViewController = DateTimePopUp(nibName: "DateTimePopUp", bundle: bundle)
           self.popViewController.delegate = self
            self.popViewController.dateMode = "end_Date"
            self.popViewController.showInView(self.view,animated: true)

            break
        default:
            break
        }
    }
    
}

extension Admin_AllAnnouncementVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return announcementList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllAnnoucementCell", for: indexPath) as! AllAnnoucementCell
        let item = announcementList[indexPath.row]
        cell.tittlelbl.text! = item["title"] as! String
        cell.descrptionlbl.text! = item["description"] as! String
        cell.datelbl.text! = item["publish_date"] as! String
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(Admin_AllAnnouncementVC.deleteBTNAction(_:)), for: .touchUpInside)

        cell.selectionStyle = .none

       return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UserDefaults.standard.bool(forKey: "is_Admin") == true{
            let item = announcementList[indexPath.row]
        let objvc = self.storyboard?.instantiateViewController(withIdentifier: "Admin_AddAnnounceVC") as? Admin_AddAnnounceVC
           objvc?.update = "announcement"
            objvc!.ides = item["id"] as? String
            objvc!.statusStr = item["status"] as? String
          self.navigationController?.pushViewController(objvc!, animated: true)
        }

    }

    
    @objc func deleteBTNAction(_ sender: UIButton) {
        print("delete")
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let item = announcementList[indexPath.row]
        let ids = item["id"] as! String
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr = baseUrl + delete_announcement
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"id":ids] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                self.get_annoncement_ApiCall()
                KAppDelegate.showAlertNotification("Announcement record has been successfully deleted")
            }
        }

    }
    
}

extension Admin_AllAnnouncementVC:DatePickerPopUpViewDelegate{
    func didPresDoneActionBtn(_ controller: DateTimePopUp, isDate: String) {
        self.startDateBtn.setTitle(isDate, for: .normal)
    }
    
    func didPresDoneTimeActionBtn(_ controller: DateTimePopUp, isTime: String) {
        print("time")
    }
    
    func didPresEndDateActionBtn(_ controller: DateTimePopUp, isendDate: String) {
        self.endDateBtn.setTitle(isendDate, for: .normal)
    }
    func didPresEndTimeActionBtn(_ controller: DateTimePopUp, isendTime: String) {
        print("time")
    }
    
    
}
