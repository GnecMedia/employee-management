//
//  Admin_AddAnnounceVC.swift
//  Employee Management
//
//  Created by vidya on 10/11/2020.
//

import UIKit
import DropDown

class Admin_AddAnnounceVC: UIViewController,DatePickerPopUpViewDelegate {
    fileprivate var popViewController :  DateTimePopUp!
    @IBOutlet weak var dateTxt:UITextField!
    @IBOutlet weak var tittleTxt:UITextField!
    @IBOutlet weak var messageTxt:UITextField!
    @IBOutlet weak var annoucementTyprBTn:UIButton!
    @IBOutlet weak var annoucementTxtBTn:UIButton!
    @IBOutlet weak var dateHeight:NSLayoutConstraint!
    @IBOutlet weak var dateViewHeight:NSLayoutConstraint!
    let dropDown = DropDown()
    var annoucement_type:String! = ""
    var menuType:String!
    var update:String! = ""
    var ides:String! = ""
    var statusStr:String! = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add Announcement"

        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
            item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
        self.dateHeight.constant = 0
        self.dateViewHeight.constant = 0
        showdropdown()
        if update == "announcement"{
            self.annoucementTxtBTn.setTitle("UPDATE", for: .normal)
            get_announcement_by_idApi()

        }
        // Do any additional setup after loading the view.
    }
    
    @objc func barButtonAction(){
        if self.menuType == "menu"{
            KAppDelegate.makeRootViewController()
        }else{
        self.navigationController?.popViewController(animated: true)
        }
    }
    func get_announcement_by_idApi(){
        let userid = UserDefaults.standard.object(forKey: "user_id")
          let accesToken = UserDefaults.standard.object(forKey: "access_token")
           let urlStr = baseUrl + get_announcement_by_id
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"id":ides!] as [String:Any]
           postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
               print("Result--\(result)")
               if succ{
                let data = result["data"] as! [String:Any]
                let dict = data["announcement_data"] as! [String:Any]
                if let publish_date = dict["publish_date"] as? String{
                    let dateStr:String = publish_date
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd/MM/yyyy"
                    let datee = dateFormatter.date(from: dateStr)
                    let dateFormatterPrint = DateFormatter()
                    dateFormatterPrint.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatterPrint.dateFormat = "yyyy-MM-dd"
                    let datee1 = dateFormatterPrint.string(from: datee!)
                    self.dateTxt.text! = "\(datee1)"
                }
               if  let publish_type = dict["publish_type"] as? String{
                if publish_type == "SCHEDULED"{
                self.dateHeight.constant = 20
                self.dateViewHeight.constant = 50
                self.annoucement_type = publish_type
                self.annoucementTyprBTn.setTitle(publish_type, for: .normal)
                }else{
                self.annoucement_type = publish_type
                self.annoucementTyprBTn.setTitle(publish_type, for: .normal)
                self.dateHeight.constant = 0
                self.dateViewHeight.constant = 0
                }
               }
                if let title = dict["title"] as? String{
                    self.tittleTxt.text! = title
                }
                if let description = dict["description"] as? String{
                    self.messageTxt.text! = description
                }
                
               }
           }
    }
    @IBAction func selectTypeBtn(sender:UIButton){
        switch sender.tag {
        case 100:
            dropDown.show()

            print("Type")
            break
        case 101:
            print("date")
            let bundle = Bundle(for: DateTimePopUp.self)
            self.popViewController = DateTimePopUp(nibName: "DateTimePopUp", bundle: bundle)
          self.popViewController.delegate = self
            self.popViewController.showInView(self.view,animated: true)

            break
        default:
            break
        }
        
    }
    func didPresDoneActionBtn(_ controller: DateTimePopUp, isDate: String) {
        self.dateTxt.text = isDate
        
    }
    func didPresEndDateActionBtn(_ controller: DateTimePopUp, isendDate: String) {
        print("date")

    }
    
    func didPresEndTimeActionBtn(_ controller: DateTimePopUp, isendTime: String) {
        print("date")

    }

    
    func didPresDoneTimeActionBtn(_ controller: DateTimePopUp, isTime: String) {
        print("date")
    }

    func showdropdown(){
        dropDown.anchorView = annoucementTyprBTn
        dropDown.dataSource = ["NOW", "SCHEDULED"]

        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            if index == 0{
                self.dateHeight.constant = 0
                self.dateViewHeight.constant = 0

            }else{
                self.dateHeight.constant = 20
                self.dateViewHeight.constant = 50

            }
            self.annoucement_type = "\(item)"
            self.annoucementTyprBTn.setTitle("\(item)", for: .normal)
        }

    }
    @IBAction func updateBtn(sender:UIButton){
        if self.annoucement_type != ""{
            if self.tittleTxt.text?.isEmpty != true{
                if self.messageTxt.text?.isEmpty != true{
                    addAnnouncementApi()

                }else{
                    KAppDelegate.showAlertNotification("Please enter message")
                }
            }else{
                KAppDelegate.showAlertNotification("Please enter Tittle")
            }
        }else{
            KAppDelegate.showAlertNotification("Please select Annoucement Type")
        }
    }
    func addAnnouncementApi(){
     let userid = UserDefaults.standard.object(forKey: "user_id")
       let accesToken = UserDefaults.standard.object(forKey: "access_token")
        var urlStr:String!
        if update == "announcement"{
            urlStr = baseUrl + update_announcement

        }else{
            urlStr = baseUrl + add_announcement
        }
        //let urlStr = baseUrl + add_announcement
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"title":self.tittleTxt.text!,"description":self.messageTxt.text!,"publish_type":self.annoucement_type!,"publish_date":self.dateTxt.text!,"id":ides!,"status":statusStr!] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                self.navigationController?.popViewController(animated: true)
                KAppDelegate.showAlertNotification(result["message"] as! String)

            }
        }

    }
    
}
extension Admin_AddAnnounceVC:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        animateViewMoving(true, moveValue: 100)

    }
    func animateViewMoving (_ up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()

    }
    func textFieldDidEndEditing(_ textField: UITextField) {
     animateViewMoving(false, moveValue: 100)
      textField.resignFirstResponder()

    }

}
