//
//  AllEmployeeVC.swift
//  Employee Management
//
//  Created by vidya on 09/11/2020.
//

import UIKit

class AllEmployeeVC: UIViewController,UITextFieldDelegate{
    @IBOutlet weak var searchTxt:UITextField!
    @IBOutlet weak var allEmpTbl:UITableView!
    var allemployeeList = [[String:Any]]()
    var filterArray = [[String:Any]]()
    var searching:Bool = false
    var menuType:String!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "All Employee"
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
            item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
        allEmployeeApi()
        // Do any additional setup after loading the view.
    }
    @objc func barButtonAction(){
        if self.menuType == "menu"{
            KAppDelegate.makeRootViewController()
        }else{
        self.navigationController?.popViewController(animated: true)
        }
    }

    func allEmployeeApi(){
        self.allemployeeList.removeAll()
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr = baseUrl + get_employee
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"emp_id":userid!] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                let data = result["data"] as? [String:Any]
                let employee_data = data!["employee_data"] as! [[String:Any]]
                for dict in employee_data{
                    self.allemployeeList.append(dict)
                }
            }
            self.allEmpTbl.reloadData()
        }
    }
    
    //MARK:- UItextFieldDelegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        //input text
        let searchText  = self.searchTxt.text! + string

        filterArray = allemployeeList.filter({ (object) -> Bool in
            let tmp:NSString = object["full_name"] as! NSString
           let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
           return range.location != NSNotFound
       })
        if(searchText.count == 0){
            searching = false
        }else{
            searching = true
            
        }
        
        if searchText.count == 1{
            searching = false
        }
        
        self.allEmpTbl.reloadData();
        
        return true
    }
    

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searchTxt.resignFirstResponder()
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        searching = true
        //self.invSummTableV.reloadData()
    }

}
extension AllEmployeeVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching {
            return self.filterArray.count

        }else{
        return self.allemployeeList.count
        }

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllEmployeeCell", for: indexPath) as! AllEmployeeCell
        cell.selectionStyle = .none
        let item:[String:Any]!
        if searching {
            item = self.filterArray[indexPath.row]
        }else{
            item = self.allemployeeList[indexPath.row]
        }
        if let employee_id = item["employee_id"] as? String{
            cell.empidlbl.text! = employee_id
        }
        if let designation = item["designation"] as? String{
            cell.designationlbl.text! = designation
        }
        if let full_name = item["full_name"] as? String{
            cell.namelbl.text! = full_name
        }
        if let profile_picture = item["profile_picture"] as? String{
            cell.profileImg.sd_setImage(with: URL(string: profile_picture))
        }
       return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item:[String:Any]!
        if searching {
            item = self.filterArray[indexPath.row]
        }else{
            item = self.allemployeeList[indexPath.row]
        }
        let objvc = self.storyboard?.instantiateViewController(withIdentifier: "EmployeeDetailVC") as? EmployeeDetailVC
        objvc?.nameStr = item["full_name"] as? String
        objvc?.designationStr = item["designation"] as? String
        objvc?.profileStr = item["profile_picture"] as? String
        objvc?.emp_id = item["emp_id"] as? String
      self.navigationController?.pushViewController(objvc!, animated: true)

    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let item = self.allemployeeList[indexPath.row]
            print("delete",item["emp_id"] as! String)
            employeeApiCall(emp_id: item["emp_id"] as! String)
            allemployeeList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            
        }
    }
    func employeeApiCall(emp_id:String){
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr = baseUrl + delete_employee
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"emp_id":emp_id] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                self.allEmployeeApi()
            }
        }
    }
}

