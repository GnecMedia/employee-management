//
//  DailyReportVC.swift
//  Employee Management
//
//  Created by vidya on 10/11/2020.
//

import UIKit

class DailyReportVC: UIViewController {
    fileprivate var popViewController :  DateTimePopUp!
    @IBOutlet weak var dailyreportTbl:UITableView!
    @IBOutlet weak var changedateBtn:UIButton!
    @IBOutlet weak var currentdateBtn:UIButton!
    var attendance_datalist = [[String:Any]]()
    var dateStr:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Today's Attendance"
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
            item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        let result = formatter.string(from: date)
        self.currentdateBtn.setTitle(result, for: .normal)
        if Reachability.isConnectedToNetwork() == true{
            get_daily_attendance_listApi(date: dateStr)
        }else{
            KAppDelegate.showAlertNotification(checkInternet)
        }
        // Do any additional setup after loading the view.
    }
    @objc func barButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func chnageBtn_action(sender:UIButton){
        let bundle = Bundle(for: DateTimePopUp.self)
        self.popViewController = DateTimePopUp(nibName: "DateTimePopUp", bundle: bundle)
      self.popViewController.delegate = self
        self.popViewController.showInView(self.view,animated: true)

    }
    
    func get_daily_attendance_listApi(date:String){
        self.attendance_datalist.removeAll()
        let userid = UserDefaults.standard.object(forKey: "user_id")
          let accesToken = UserDefaults.standard.object(forKey: "access_token")
           let urlStr = baseUrl + get_daily_attendance_list
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"filter_date":date] as [String:Any]
           postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
               print("Result--\(result)")
               if succ{
                let data = result["data"] as! [String:Any]
                let attendance_data = data["attendance_data"] as! [[String:Any]]
                for dict in attendance_data{
                    self.attendance_datalist.append(dict)
                }
               }
            self.dailyreportTbl.reloadData()
           }


    }
}
extension DailyReportVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.attendance_datalist.count
    

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DailyReportCell", for: indexPath) as! DailyReportCell
        cell.selectionStyle = .none
        let item = attendance_datalist[indexPath.row]
        cell.namelbl.text! = item["full_name"] as! String
        cell.datelbl.text! = item["logged_in_date"] as! String
        var logintime:String! = ""
        var loginOuttime:String! = "null"

        if let logged_in_time = item["logged_in_time"] as? String{
            logintime = logged_in_time
        }
        if let logged_out_time = item["logged_out_time"] as? String{
            loginOuttime = logged_out_time
        }
        cell.timelbl.text! = logintime + "-" + loginOuttime

       return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

extension DailyReportVC:DatePickerPopUpViewDelegate{
    func didPresDoneActionBtn(_ controller: DateTimePopUp, isDate: String) {
        self.changedateBtn.setTitle(isDate, for: .normal)
        get_daily_attendance_listApi(date: isDate)
    }
    
    func didPresDoneTimeActionBtn(_ controller: DateTimePopUp, isTime: String) {
        print("t")
    }
    
    func didPresEndDateActionBtn(_ controller: DateTimePopUp, isendDate: String) {
        print("t")

    }
    
    func didPresEndTimeActionBtn(_ controller: DateTimePopUp, isendTime: String) {
        print("t")

    }
    
    
}
