//
//  AddHolidayVC.swift
//  Employee Management
//
//  Created by vidya on 10/11/2020.
//

import UIKit
import DLRadioButton

class AddHolidayVC: UIViewController,UITextFieldDelegate {
    fileprivate var popViewController :  DateTimePopUp!
    @IBOutlet weak var holidaynameTxt:UITextField!
    @IBOutlet weak var holidaydateTxt:UITextField!
    var statusStr:String! = ""
    var updateData = [String:Any]()
    @IBOutlet var yesButton : DLRadioButton!
    @IBOutlet var noButton : DLRadioButton!
    var upadateStatus:String!
    var menuType:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add Holiday"
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
            item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item

        if upadateStatus == "update"{
        self.holidaynameTxt.text! = updateData["festival"] as! String
        self.holidaydateTxt.text! = updateData["holiday_date"] as! String
        if updateData["status"] as! String == "1"{
            for radioButton in self.noButton.otherButtons {
                radioButton.isSelected = true;
                self.statusStr = "0"
            }
        }else if updateData["status"] as! String == "0"{
            for radioButton in self.yesButton.otherButtons {
                radioButton.isSelected = true;
                self.statusStr = "1"

            }
        }
        }
        
        // Do any additional setup after loading the view.
    }
    @objc func barButtonAction(){
        if self.menuType == "menu"{
            KAppDelegate.makeRootViewController()
        }else{
        self.navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func selectedBtn_Action(sender:UIButton){
        let bundle = Bundle(for: DateTimePopUp.self)
        self.popViewController = DateTimePopUp(nibName: "DateTimePopUp", bundle: bundle)
      self.popViewController.delegate = self
        self.popViewController.showInView(self.view,animated: true)


    }
    @IBAction func selectedButton(radioButton : DLRadioButton) {
     let seletedbtn = radioButton.selected()!.titleLabel!.text!
        print("seletedbtn-\(seletedbtn)")
        if seletedbtn == "Yes"{
            self.statusStr = "1"
        }else{
            self.statusStr  = "0"
        }
//        self.statusStr = seletedbtn
    }
    @IBAction func addBtn_Action(sender:UIButton){
        if upadateStatus == "update"{
            if Reachability.isConnectedToNetwork() == true{
            update_updateApiCall()
            }else{
                KAppDelegate.showAlertNotification(checkInternet)
            }
        }else{
            if statusStr != ""{
            if self.holidaynameTxt.text?.isEmpty != true{
                if self.holidaydateTxt.text?.isEmpty != true{
                    if Reachability.isConnectedToNetwork() == true{
                    add_holidayApi()
                    }else{
                        KAppDelegate.showAlertNotification(checkInternet)
                    }

                }else{
                    KAppDelegate.showAlertNotification("Please enter Holiday date")

                }
            }else{
                KAppDelegate.showAlertNotification("Please enter Festival")
            }
            }else{
                KAppDelegate.showAlertNotification("Please select show")

            }
        }
    }
    func update_updateApiCall()
    {
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
         let urlStr = baseUrl + update_holiday
        let parms = ["user_id":userid!,"access_token":accesToken!,"festival":self.holidaynameTxt.text!,"holiday_date":self.holidaydateTxt.text!,"status":self.statusStr!,"id":updateData["id"] as! String] as [String:Any]
         postAPiwithOut(urlStr: urlStr, parameter: parms){(succ, result) in
             print("Result--\(result)")
             if succ{
              self.navigationController?.popViewController(animated: true)
             }
         }

        
    }
    func add_holidayApi(){
        let userid = UserDefaults.standard.object(forKey: "user_id")
          let accesToken = UserDefaults.standard.object(forKey: "access_token")
           let urlStr = baseUrl + add_holiday
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"festival":self.holidaynameTxt.text!,"holiday_date":self.holidaydateTxt.text!,"status":self.statusStr!] as [String:Any]
           postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
               print("Result--\(result)")
               if succ{
                self.navigationController?.popViewController(animated: true)
               }
           }

    }
    

}
extension AddHolidayVC:DatePickerPopUpViewDelegate{
    func didPresDoneActionBtn(_ controller: DateTimePopUp, isDate: String) {
        self.holidaydateTxt.text! = isDate
    }
    
    func didPresDoneTimeActionBtn(_ controller: DateTimePopUp, isTime: String) {
        print("date")
    }
    
    func didPresEndDateActionBtn(_ controller: DateTimePopUp, isendDate: String) {
        print("date")

    }
    
    func didPresEndTimeActionBtn(_ controller: DateTimePopUp, isendTime: String) {
        print("date")

    }
    
    
}
