//
//  HomeVC.swift
//  Employee Management
//
//  Created by vidya on 12/10/2020.
//

import UIKit

class HomeVC: SideBaseViewController {
    @IBOutlet weak var homeCollectionView:UICollectionView!
    var mImageScrollView: UIScrollView! = UIScrollView()
    var customView:UIView = UIView()
    var pageControl : UIPageControl! = UIPageControl()
    let modelName = UIDevice.modelName

    let imageArray = ["dash_mark_attendance","apply_leave","holiday_list","upload_bills","dash_report","mark_attendance","dash_announcement"]
    let tittleArray = ["Mark Attendance","Leave","Holiday List","Upload Bills","Report","Project","Announcement"]
    var homePageBanners = ["banner1","banner2","banner3","banner4","banner5"]
    var adminImage = ["dash_all_employee","dash_announcement","holiday_list","upload_bills","dash_report","dash_mark_attendance","dash_today_attendance"]
    var admintittle = ["All Employee","Announcement","Holiday List","Upload Bills","Report","Mark Attendance","Today's Attendance"]

//    let myImageView = UIImageView()

    
    
    override func viewDidLoad() {
    super.viewDidLoad()
      // fatalError()

        navigationController?.navigationBar.topItem?.title = "Home"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.isHidden = false
        let color2 = UIColor(rgb: 0x73AFBA)
        self.navigationController?.navigationBar.barTintColor = color2

        self.homeCollectionView.register(UINib(nibName: "HomeHeaderCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderIdentifer")
        self.customView = UIView(frame: CGRect(x: 10, y: 0, width: self.homeCollectionView.frame.width, height: 180))
        self.mImageScrollView = UIScrollView(frame:CGRect(x: 0, y: 0, width: self.homeCollectionView.frame.width , height: 180))
        if modelName == "iPhone 5" || modelName == "iPhone 5s" {
            self.customView = UIView(frame: CGRect(x: 10, y: 0, width: self.homeCollectionView.frame.width - 80, height: 180))
            self.mImageScrollView = UIScrollView(frame:CGRect(x: 0, y: 0, width: self.homeCollectionView.frame.width - 80 , height: 180))
        }else if modelName == "iPhone 7 Plus" || modelName == "iPhone 6 Plus" || modelName == "iPhone 6s Plus" || modelName == "iPhone 8 Plus" || modelName == "iPhone 11" || modelName == "iPhone 11 Pro Max" || modelName == "iPhone 12" || modelName == "iPhone 12 Pro Max" || modelName == "iPhone XS Max" || modelName == "iPhone XR"{
            self.customView = UIView(frame: CGRect(x: 10, y: 0, width: self.homeCollectionView.frame.width, height: 180))
            self.mImageScrollView = UIScrollView(frame:CGRect(x: 10, y: 0, width: self.homeCollectionView.frame.width, height: 180))

        }else{
            self.customView = UIView(frame: CGRect(x: 10, y: 0, width: self.homeCollectionView.frame.width - 20, height: 180))
            self.mImageScrollView = UIScrollView(frame:CGRect(x: 0, y: 0, width: self.homeCollectionView.frame.width - 20  , height: 180))

        }
//        let screenWidth = UIScreen.main.bounds.size.width
//        print("screenWidth--\(screenWidth)")

        self.customView.addSubview(mImageScrollView)
        mImageScrollView.isPagingEnabled = true
        self.homeCollectionView.addSubview(self.customView)
        silderCall()
        // Do any additional setup after loading the view.
    }
    
    @objc func barButtonAction() {
        //self.navigationController?.popViewController(animated: true)

    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(slideanimation), userInfo: nil, repeats: true)
    }
    @objc func slideanimation(){
        let contentOffset:CGFloat = mImageScrollView.contentOffset.x;
        var nextPage:Int = (Int)(contentOffset/mImageScrollView.frame.size.width) + 1

        if modelName == "iPhone 5" || modelName == "iPhone 5s" {
            if  nextPage < homePageBanners.count {
                
                mImageScrollView.scrollRectToVisible(CGRect(x: CGFloat(nextPage) * mImageScrollView.frame.size.width - 25, y: 0, width: self.homeCollectionView.frame.width, height:180), animated: true)
                
                pageControl.currentPage = nextPage;
                
            }else{
                mImageScrollView.scrollRectToVisible(CGRect(x: 0, y: 0, width: self.homeCollectionView.frame.width - 25, height:180), animated: true)
                nextPage = 0
                pageControl.currentPage=nextPage;
            }
        }else if modelName == "iPhone 7 Plus" || modelName == "iPhone 6 Plus" || modelName == "iPhone 6s Plus" || modelName == "iPhone 8 Plus" || modelName == "iPhone 11" || modelName == "iPhone 11 Pro Max" || modelName == "iPhone 12 Pro Max" || modelName == "iPhone 12" || modelName == "iPhone XS Max" || modelName == "iPhone XR"{
            if  nextPage < homePageBanners.count {

                mImageScrollView.scrollRectToVisible(CGRect(x: CGFloat(nextPage) * mImageScrollView.frame.size.width - 40, y: 0, width: self.homeCollectionView.frame.width, height:180), animated: true)
                pageControl.currentPage = nextPage;
                
            }else{
                mImageScrollView.scrollRectToVisible(CGRect(x: 0, y: 0, width: self.homeCollectionView.frame.width - 40, height:180), animated: true)
                nextPage = 0
                pageControl.currentPage=nextPage;
            }
                        
        }else{
            if  nextPage < homePageBanners.count {

                mImageScrollView.scrollRectToVisible(CGRect(x: CGFloat(nextPage) * mImageScrollView.frame.size.width - 20, y: 0, width: self.homeCollectionView.frame.width, height:180), animated: true)
                pageControl.currentPage = nextPage;
                
            }else{
                mImageScrollView.scrollRectToVisible(CGRect(x: 0, y: 0, width: self.homeCollectionView.frame.width - 20, height:180), animated: true)
                nextPage = 0
                pageControl.currentPage=nextPage;
            }
            

        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollview")
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    

    func silderCall() {
        self.mImageScrollView.delegate = self
        var imageWidth:CGFloat?
        var imageHeight:CGFloat?
        print("modelName--\(modelName)")

        if modelName == "iPhone 5" || modelName == "iPhone 5s" {//320
            imageWidth = self.homeCollectionView.frame.width - 80
        }else if modelName == "iPhone 7 Plus" || modelName == "iPhone 6 Plus" || modelName == "iPhone 6s Plus" || modelName == "iPhone 8 Plus" || modelName == "iPhone 11" || modelName == "iPhone 11 Pro Max" || modelName == "iPhone 12" || modelName == "iPhone 12 Pro Max" || modelName == "iPhone XS Max" || modelName == "iPhone XR"{//414
            imageWidth = self.homeCollectionView.frame.width

        }else{//375
            imageWidth = self.homeCollectionView.frame.width - 20

        }

        imageHeight = 180
        
        var xPosition:CGFloat = 0
        var scrollViewSize:CGFloat = 0
        for image in homePageBanners{
            let myImageView = UIImageView()
            myImageView.image = UIImage(named: image)
            myImageView.layer.cornerRadius = 10
            myImageView.layer.masksToBounds = true
           // myImageView.contentMode = .scaleAspectFit
            myImageView.frame.size.width = imageWidth!
            myImageView.frame.size.height = CGFloat(imageHeight!)
            myImageView.frame.origin.x = xPosition
            myImageView.frame.origin.y = 0
            self.mImageScrollView.addSubview(myImageView)
            xPosition += imageWidth!
            scrollViewSize += imageWidth!
            mImageScrollView.isScrollEnabled = true

        }
        pageControl = UIPageControl(frame:CGRect(x:80, y: 140, width: 200, height: 30))
        mImageScrollView.bounces = false
        pageControl.numberOfPages = homePageBanners.count
        pageControl.currentPage = 0
//        pageControl.backgroundColor = UIColor.red
        pageControl.pageIndicatorTintColor =  UIColor.gray
        pageControl.currentPageIndicatorTintColor = UIColor.white

        self.customView.addSubview(pageControl)
        self.mImageScrollView.contentSize = CGSize(width: scrollViewSize, height: imageHeight!)

        mImageScrollView.bounces = false

    }

}
extension HomeVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
           return 1
       }

       func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if UserDefaults.standard.bool(forKey: "is_Admin") == true{
        return adminImage.count
        }else{
            return imageArray.count
        }
       }
    
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath) as! HomeCell
        if UserDefaults.standard.bool(forKey: "is_Admin") == true{
            cell.namelistlbl.text! = admintittle[indexPath.row]
            cell.listImg.image = UIImage(named: "\(adminImage[indexPath.row])")

        }else{
        cell.namelistlbl.text! = tittleArray[indexPath.row]
        cell.listImg.image = UIImage(named: "\(imageArray[indexPath.row])")
        }
        return cell

       }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if UserDefaults.standard.bool(forKey: "is_Admin") == true{
            if indexPath.item == 0{
                let objvc = self.storyboard?.instantiateViewController(withIdentifier: "AllEmployeeVC") as? AllEmployeeVC
              self.navigationController?.pushViewController(objvc!, animated: true)

            }else if indexPath.item == 1{
                let objvc = self.storyboard?.instantiateViewController(withIdentifier: "Admin_AllAnnouncementVC") as? Admin_AllAnnouncementVC
              self.navigationController?.pushViewController(objvc!, animated: true)

            }else if indexPath.item == 2{
                let objvc = self.storyboard?.instantiateViewController(withIdentifier: "HolidayVC") as? HolidayVC
              self.navigationController?.pushViewController(objvc!, animated: true)
            }else if indexPath.item == 3{
                let objvc = self.storyboard?.instantiateViewController(withIdentifier: "AllBillsVC") as? AllBillsVC
              self.navigationController?.pushViewController(objvc!, animated: true)

                }else if indexPath.item == 4{
                let objvc = self.storyboard?.instantiateViewController(withIdentifier: "ReportVC") as? ReportVC
              self.navigationController?.pushViewController(objvc!, animated: true)
                }else if indexPath.item == 5{
                    let objvc = self.storyboard?.instantiateViewController(withIdentifier: "MarkAttendanceVC") as? MarkAttendanceVC
                  self.navigationController?.pushViewController(objvc!, animated: true)
                }else if indexPath.item == 6{
                    let objvc = self.storyboard?.instantiateViewController(withIdentifier: "DailyReportVC") as? DailyReportVC
                  self.navigationController?.pushViewController(objvc!, animated: true)

                }
            
        }else{
        if indexPath.item == 0{
            let objvc = self.storyboard?.instantiateViewController(withIdentifier: "MarkAttendanceVC") as? MarkAttendanceVC
          self.navigationController?.pushViewController(objvc!, animated: true)

        }else if indexPath.item == 1{
      let objvc = self.storyboard?.instantiateViewController(withIdentifier: "AllLeaveVC") as? AllLeaveVC
    self.navigationController?.pushViewController(objvc!, animated: true)
        }else if indexPath.item == 2{
            let objvc = self.storyboard?.instantiateViewController(withIdentifier: "HolidayVC") as? HolidayVC
          self.navigationController?.pushViewController(objvc!, animated: true)

        }else if indexPath.item == 3{
            let objvc = self.storyboard?.instantiateViewController(withIdentifier: "AllBillsVC") as? AllBillsVC
          self.navigationController?.pushViewController(objvc!, animated: true)
        }else if indexPath.item == 4{
            let objvc = self.storyboard?.instantiateViewController(withIdentifier: "ReportVC") as? ReportVC
          self.navigationController?.pushViewController(objvc!, animated: true)
        }else if indexPath.item == 5{
            let objvc = self.storyboard?.instantiateViewController(withIdentifier: "ProjectVC") as? ProjectVC
          self.navigationController?.pushViewController(objvc!, animated: true)
        }else if indexPath.item == 6{
            let objvc = self.storyboard?.instantiateViewController(withIdentifier: "Admin_AllAnnouncementVC") as? Admin_AllAnnouncementVC
          self.navigationController?.pushViewController(objvc!, animated: true)


        }
        }

    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
     {
         
         switch kind {
         case UICollectionView.elementKindSectionHeader:
             let headerView = homeCollectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderIdentifer", for: indexPath) as! HomeHeaderCell

             return headerView
             
         default:
             return UICollectionReusableView()
         }
     }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewsize = homeCollectionView.bounds.width
            
            return CGSize(width: collectionViewsize / 2 - 10 , height: collectionViewsize / 2 - 10)
    }

}
