//
//  NotificationVC.swift
//  Employee Management
//
//  Created by vidya on 19/10/2020.
//

import UIKit

class NotificationVC: SideBaseViewController {
    @IBOutlet weak var notificationTbl:UITableView!
    var notifictaionlist = [[String:Any]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.topItem?.title = "Notification"
        if Reachability.isConnectedToNetwork() == true{
           notificationApiCall()
        }else{
            KAppDelegate.showAlertNotification(checkInternet)
        }
        // Do any additional setup after loading the view.
    }
    func notificationApiCall(){
        self.notifictaionlist.removeAll()
            let userid = UserDefaults.standard.object(forKey: "user_id")
            let accesToken = UserDefaults.standard.object(forKey: "access_token")
            let urlStr = baseUrl + get_notification
            let leaveDetials = ["user_id":userid!,"access_token":accesToken!] as [String:Any]
            postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
                print("Result--\(result)")
                if succ{
                    let data = result["data"] as? [String:Any]
                    let notificationdata = data!["notification_data"] as! [[String:Any]]
                    for dic in notificationdata{
                        self.notifictaionlist.append(dic)
                    }
                }
                self.notificationTbl.reloadData()
            }
        }



}
extension NotificationVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.notifictaionlist.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        cell.selectionStyle = .none

        let item = notifictaionlist[indexPath.row]
        if let title = item["title"] as? String{
            cell.tittleLbl.text! = title
        }
        if let body = item["body"] as? String{
            cell.detailLbl.text! = body
        }
        if let created_date = item["created_date"] as? String{
            cell.dateLbl.text! = created_date
        }
        let holdToDelete = UILongPressGestureRecognizer(target: self, action: #selector(NotificationVC.deleteBTNAction(_:)));
        holdToDelete.minimumPressDuration = 1.00;
          cell.tag = indexPath.row
        cell.addGestureRecognizer(holdToDelete);
       return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        

    }
    @objc func deleteBTNAction(_ sender: UILongPressGestureRecognizer) {
        print("delete")
        let tag = sender.view!.tag
        let indexPath = IndexPath(row: tag, section: 0)
        let item = notifictaionlist[indexPath.row]
        let ids = item["id"] as! String
        shownotificationAlert(id: ids)

    }

    func shownotificationAlert(id:String) {
        let alert = UIAlertController(title: "", message: "Are You Sure Want to remove this Notification",preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
        }))
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
         print("Ok")
       self.deleteApiCall(id: id)
      }))
        self.present(alert, animated: true, completion: nil)
    }

    func deleteApiCall(id:String){
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr = baseUrl + delete_notification
        let parms = ["user_id":userid!,"access_token":accesToken!,"id":id] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: parms){(succ, result) in
            print("Result--\(result)")
            if succ{
                KAppDelegate.showAlertNotification("Notification record has been successfully deleted")
                self.notificationApiCall()
            }
        }

    }

}
