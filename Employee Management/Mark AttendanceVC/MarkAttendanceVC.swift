//
//  MarkAttendanceVC.swift
//  Employee Management
//
//  Created by vidya on 28/10/2020.
//

import UIKit
import MapKit
import CoreLocation

class MarkAttendanceVC: UIViewController,CLLocationManagerDelegate , MKMapViewDelegate
 {
    @IBOutlet weak var dateLbl:UILabel!
    let locationManager = CLLocationManager()
    @IBOutlet weak var mapview:MKMapView!
    var currentLocationStr:String!
    var lat:Double!
    var long:Double!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Mark Attendance"
    let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
        item.tintColor = UIColor.white
    self.navigationItem.leftBarButtonItem = item
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "dd-MMM-yyyy"
        let formattedDate = format.string(from: date)
        print(formattedDate)
        self.dateLbl.text = "\(formattedDate)"
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        mapview.delegate = self
        mapview.mapType = .standard
        mapview.isZoomEnabled = true
        mapview.isScrollEnabled = true

        if let coor = mapview.userLocation.location?.coordinate{
            mapview.setCenter(coor, animated: true)
        }

        

        // Do any additional setup after loading the view.
    }
  @IBAction  func monthlyReportBtn_Action(sender:UIButton){
        let objvc = self.storyboard?.instantiateViewController(withIdentifier: "ReportVC") as? ReportVC
        self.navigationController?.pushViewController(objvc!, animated: true)


    }
    @IBAction func attendanceMark_Action(sender:UIButton){
        if Reachability.isConnectedToNetwork() == true{
        attendenceMarkApicall()
        }else{
            KAppDelegate.showAlertNotification(checkInternet)
        }
    }
    @IBAction func attendanceOut_Action(sender:UIButton){
        if Reachability.isConnectedToNetwork() == true{
        attendenceLogOutApi()
        }else{
            KAppDelegate.showAlertNotification(checkInternet)
        }
    }
    
    func attendenceMarkApicall(){
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        print("deviceIDlogin--\(deviceID)")//"52C687E7-5D86-42B8-B7E3-454B6E70A813"

        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr = baseUrl + mark_attendance_in
        let attendanceMarkDetials = ["user_id":userid!,"access_token":accesToken!,"latitude":lat!,"longitude":long!,"device_imei":deviceID] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: attendanceMarkDetials){(succ, result) in
            if succ{
                self.navigationController?.popViewController(animated: true)
                KAppDelegate.showAlertNotification(result["message"] as! String)
            }
        }
    }
    func attendenceLogOutApi(){
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        print("deviceIDout--\(deviceID)")//"52C687E7-5D86-42B8-B7E3-454B6E70A813"

        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr = baseUrl + mark_attendance_out
        let attendanceMarkDetials = ["user_id":userid!,"access_token":accesToken!,"latitude":lat!,"longitude":long!,"device_imei":deviceID] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: attendanceMarkDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                self.navigationController?.popViewController(animated: true)
                KAppDelegate.showAlertNotification(result["message"] as! String)
            }
        }

    }
    

    //MARK:- CLLocationManagerDelegate Methods

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let mUserLocation:CLLocation = locations[0] as CLLocation

        let center = CLLocationCoordinate2D(latitude: mUserLocation.coordinate.latitude, longitude: mUserLocation.coordinate.longitude)
        let mRegion = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        getAddressFromLatLon(mLattitude: mUserLocation.coordinate.latitude, mLongitude: mUserLocation.coordinate.longitude)
       /* let mkAnnotation: MKPointAnnotation = MKPointAnnotation()
            mkAnnotation.coordinate = CLLocationCoordinate2DMake(mUserLocation.coordinate.latitude, mUserLocation.coordinate.longitude)
        print("Map--\(currentLocationStr)")
        DispatchQueue.main.async {
            mkAnnotation.title = self.currentLocationStr
           self.mapview.addAnnotation(mkAnnotation)
        }*/
        mapview.setRegion(mRegion, animated: true)

    }
func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error - locationManager: \(error.localizedDescription)")
    }

    
    func getAddressFromLatLon(mLattitude: CLLocationDegrees, mLongitude: CLLocationDegrees) {

            var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
            let lat: Double = mLattitude//Double("\(pdblLatitude)")!
            //21.228124
            let lon: Double = mLongitude//Double("\(pdblLongitude)")!
            //72.833770
//            print("lon-\(lon)")
//            print("lat-\(lat)")
             self.lat = lat
            self.long = lon
            let ceo: CLGeocoder = CLGeocoder()
            center.latitude = lat
            center.longitude = lon

            let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)


            ceo.reverseGeocodeLocation(loc, completionHandler:
                {(placemarks, error) in
                    if (error != nil)
                    {
                        print("reverse geodcode fail: \(error!.localizedDescription)")
                    }
                    let pm = placemarks! as [CLPlacemark]

                    if pm.count > 0 {
                        let pm = placemarks![0]
//                        print(pm.country)
//                        print(pm.locality)
//                        print(pm.subLocality)
//                        print(pm.thoroughfare)
//                        print(pm.postalCode)
//                        print(pm.subThoroughfare)
                        var addressString : String = ""
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                        }
                        self.currentLocationStr = "\(addressString)"
                        let mkAnnotation: MKPointAnnotation = MKPointAnnotation()
                        mkAnnotation.coordinate = CLLocationCoordinate2DMake(mLattitude, mLongitude)
                        mkAnnotation.title = self.currentLocationStr
                        self.mapview.addAnnotation(mkAnnotation)

                        print("currentLocationStr-\(addressString)")
                  }
            })

        }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }

        let identifier = "Annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)

        if annotationView == nil {
            print("anotion")
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.image = UIImage(named: "location-on")
             annotationView!.canShowCallout = true

        } else {
            annotationView!.annotation = annotation
            //annotationView?.image = UIImage(named: "location-on")

        }
        //annotationView?.image = UIImage(named: "location-on")

        return annotationView
    }

    @objc func barButtonAction(){
        KAppDelegate.makeRootViewController()

    }

}
