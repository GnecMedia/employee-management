//
//  AppConstants.swift
//  Employee Management
//
//  Created by vidya on 28/10/2020.
//

import Foundation
import RAMAnimatedTabBarController

let KAppDelegate = AppDelegate().sharedInstance()

let baseUrl = "http://stagingemp.gnecclienttesting.com/api/v1/"

let login = "login"
let logout = "logout"
let forgot_password = "forgot_password"
let reset_password = "reset_password"
let change_password = "change_password"

let mark_attendance_in = "mark_attendance_in"
let mark_attendance_out = "mark_attendance_out"

let get_employee_by_id = "get_employee_by_id"
let get_employee = "get_employee"
let update_employee = "update_employee"

//MARK:leave Api
let apply_leave = "apply_leave"
let get_my_leave_application = "get_my_leave_application"
let delete_leave = "delete_leave"
let update_leave = "update_leave"
let get_leave_by_id = "get_leave_by_id"


//Mark:Holiday
let holiday_list = "holiday_list"
let update_holiday = "update_holiday"
let delete_holiday = "delete_holiday"

//Mark:Bill
let upload_bill_record = "upload_bill_record"
let get_my_uploaded_bills = "get_my_uploaded_bills"
let update_bill = "update_bill"
let get_bill_by_id = "get_bill_by_id"
let delete_bill = "delete_bill"

let get_announcement = "get_announcement"
let get_report = "get_report"
let get_report_detail = "get_report_detail"

//admin endpoint
let add_employee = "add_employee"
let delete_employee = "delete_employee"
 let add_announcement = "add_announcement"
let delete_announcement = "delete_announcement"
let update_announcement = "update_announcement"
let get_announcement_by_id = "get_announcement_by_id"

let add_holiday = "add_holiday"
let get_daily_attendance_list = "get_daily_attendance_list"
let clear_imei = "clear_imei"

//Notification
let get_notification = "get_notification"
let delete_notification  = "delete_notification"


let checkInternet = "No internet connect"

extension UIImage {
    func resizeWithPercent(_ percentage: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    
    func resizeWithWidth(_ width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}
