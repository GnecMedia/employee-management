//
//  UploadBillsVC.swift
//  Employee Management
//
//  Created by vidya on 28/10/2020.
//

import UIKit
import MobileCoreServices
import Photos
import Foundation
var pdfData:Data!
class UploadBillsVC: UIViewController ,UITextFieldDelegate{
    fileprivate var popViewController :  DateTimePopUp!

    @IBOutlet weak var amountTxt:UITextField!
    @IBOutlet weak var projectNameTxt:UITextField!
    @IBOutlet weak var locationTxt:UITextField!
    @IBOutlet weak var datelbl:UITextField!
    @IBOutlet weak var attactedlbl:UILabel!
   var postImageView = UIImageView()
    var menuType:String!
    var FileUpload = [UIImage]()
    var fileType:String! = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Upload Bills"
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
            item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
//        checkPermission()
        // Do any additional setup after loading the view.
    }
    @IBAction func dateBtnAction(sender:UIButton){
        let bundle = Bundle(for: DateTimePopUp.self)
        self.popViewController = DateTimePopUp(nibName: "DateTimePopUp", bundle: bundle)
       self.popViewController.delegate = self
        self.popViewController.showInView(self.view,animated: true)
            
    }
    func checkPermission() {
      let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            print("Access is granted by user")
        case .notDetermined: PHPhotoLibrary.requestAuthorization({
          (newStatus) in print("status is \(newStatus)")
            if newStatus == PHAuthorizationStatus.authorized {
            print("success") }
        })
        case .restricted:
             print("User do not have access to photo album.")
        case .denied:
             print("User has denied the permission.")
        case .limited:
            print("limit")
        @unknown default:
            print("default")

        }
    }

    @IBAction func uploadBtnAction(sender:UIButton){
        
        if self.amountTxt.text?.isEmpty != true{
            if self.datelbl.text?.isEmpty != true{
                if self.projectNameTxt.text?.isEmpty != true{
                    if self.locationTxt.text?.isEmpty != true{
                        if Reachability.isConnectedToNetwork() == true{
                        billuploadApiCall()
                        }else{
                            KAppDelegate.showAlertNotification(checkInternet)
                        }
                    }else{
                        KAppDelegate.showAlertNotification("Please Enter Location")
                    }
                }else{
                    KAppDelegate.showAlertNotification("Please Enter Project Name")
                }
            }else{
                KAppDelegate.showAlertNotification("Please Enter Bill Date")
            }
        }else{
            KAppDelegate.showAlertNotification("Please Enter Amount")
        }
    }
    
    func billuploadApiCall(){
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        var uploadedAttImg:UIImage! //= #imageLiteral(resourceName: "profile")
        if postImageView.image != nil {
        let pngImage = postImageView.image!.jpegData(compressionQuality: 0.5)
         uploadedAttImg = UIImage(data: (pngImage! as NSData) as Data)
        }
        let url = baseUrl + upload_bill_record
        let  parameter = ["user_id":userid!,"access_token":accesToken!,"bill_date":self.datelbl.text!,"bill_amount":self.amountTxt.text!,"client_project_name":projectNameTxt.text!,"location":self.locationTxt.text!] as [String:Any]
        postBillUploadData(urlStr:url, fileType: fileType, parameter: parameter, selectedevent: uploadedAttImg){(succ, result) in
            print("result---\(result)")
            if succ{
                self.navigationController?.popViewController(animated: true)
                KAppDelegate.showAlertNotification(result["message"] as! String)
            }
        }

    }

    @objc func barButtonAction(){
        if self.menuType == "menu"{
            KAppDelegate.makeRootViewController()
        }else{
        self.navigationController?.popViewController(animated: true)
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
extension UploadBillsVC:DatePickerPopUpViewDelegate{
    func didPresDoneActionBtn(_ controller: DateTimePopUp, isDate: String) {
        self.datelbl.text! = isDate
    }
    
    func didPresDoneTimeActionBtn(_ controller: DateTimePopUp, isTime: String) {
        print("isTime")
    }
    
    func didPresEndDateActionBtn(_ controller: DateTimePopUp, isendDate: String) {
        print("isTime")

    }
    
    func didPresEndTimeActionBtn(_ controller: DateTimePopUp, isendTime: String) {
        print("isTime")

    }
    
    
}

extension UploadBillsVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIDocumentPickerDelegate{

    @IBAction func attactedBtnAction(sender:UIButton){
        checkPermission()

    let alert = UIAlertController(title: "Select Image From", message: "", preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction(title: "Pdf", style: .default , handler:{ (UIAlertAction)in
                    
                    let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
                    documentPicker.delegate = self
                    documentPicker.modalPresentationStyle = .fullScreen
                    self.present(documentPicker, animated: true, completion: nil)
                }))

                alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
                    let imagePickerController = UIImagePickerController()
                    imagePickerController.sourceType = .photoLibrary

                    imagePickerController.delegate = self

                  self.view.window!.rootViewController?.present(imagePickerController, animated: true, completion: nil)
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                }))
                self.present(alert, animated: true, completion: {
                })
  }
     func imagePickerController(_ picker: UIImagePickerController,didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
         let image = info[.originalImage] as! UIImage
          postImageView.image = image
        print("image--\(image)")
        if let asset = info[UIImagePickerController.InfoKey.phAsset] as? PHAsset {
            let assetResources = PHAssetResource.assetResources(for: asset)
        let originalfilename = assetResources.first!.originalFilename
            self.attactedlbl.text! = originalfilename
            print(assetResources.first!.originalFilename)
        }

         picker.dismiss(animated: true, completion: nil)

     }
     func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
         picker.dismiss(animated: true, completion: nil)
     }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
    // Dismiss the picker if the user canceled.
    dismiss(animated: true, completion: nil)
    }

    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        let fileName = myURL.lastPathComponent
        print("import result : \(myURL)")
        let pdfData1 = try! Data(contentsOf: myURL)
         pdfData = pdfData1
        
       // let urlConverted = drawPDFfromURL(url: myURL)!
        print("urlConverted : \(pdfData!)")
       // self.postImageView.image = urlConverted
        self.attactedlbl.text = fileName
        self.fileType = "pdf"

    }
    func drawPDFfromURL(url: URL) -> UIImage? {
         guard let document = CGPDFDocument(url as CFURL) else { return nil }
        print("document--\(document)")
         guard let page = document.page(at:1) else { return nil }
        print("page--\(page)")

         let pageRect = page.getBoxRect(.mediaBox)
         let renderer = UIGraphicsImageRenderer(size: pageRect.size)
         let img = renderer.image { ctx in
             UIColor.white.set()
             ctx.fill(pageRect)
             ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
             ctx.cgContext.scaleBy(x: 1.0, y: -1.0)
             ctx.cgContext.drawPDFPage(page)
         }
     
         return img
     
     }


}
