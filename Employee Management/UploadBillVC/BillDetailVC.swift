//
//  BillDetailVC.swift
//  Employee Management
//
//  Created by vidya on 06/11/2020.
//

import UIKit
import WebKit

class BillDetailVC: UIViewController,WKUIDelegate {
    @IBOutlet weak var mwebView:WKWebView!
    @IBOutlet weak var txv_bill_date:UILabel!
    @IBOutlet weak var txv_bill_amount:UILabel!
    @IBOutlet weak var txv_project_name:UILabel!
    @IBOutlet weak var txv_location:UILabel!

    var file_url:String! = ""
    var alldetails = [String:Any]()
    var my_url:URL!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Bill Details"
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
            item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
       /* if file_url != ""{
            let myURL = URL(string:file_url)
            let myRequest = URLRequest(url: myURL!)
            mwebView.load(myRequest)
        }
       if let bill_amount = alldetails["bill_amount"] as? String{
        self.txv_bill_amount.text! = "Bill Amount:-" + bill_amount
       }
        if let bill_date = alldetails["bill_date"] as? String{
         self.txv_bill_date.text! = "Bill Date:-" + bill_date
        }
        if let client_project_name = alldetails["client_project_name"] as? String{
         self.txv_project_name.text! = "Project Name:-" + client_project_name
        }
        if let location = alldetails["location"] as? String{
         self.txv_location.text! = "Location:-" + location
        }*/
        get_bill_by_idApicall()
        // Do any additional setup after loading the view.
    }
    func get_bill_by_idApicall(){
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let ids = alldetails["id"] as? String
        let urlStr = baseUrl + get_bill_by_id
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"id":ids!] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                let data = result["data"] as? [String:Any]
                let bill_data = data!["bill_data"] as! [String:Any]
                if let bill_amount = bill_data["bill_amount"] as? String{
                    self.txv_bill_amount.text! = "Bill Amount:-" + bill_amount
                }
                if let bill_date = bill_data["bill_date"] as? String{
                 self.txv_bill_date.text! = "Bill Date:-" + bill_date
                }
                if let client_project_name = bill_data["client_project_name"] as? String{
                 self.txv_project_name.text! = "Project Name:-" + client_project_name
                }
                if let location = bill_data["location"] as? String{
                 self.txv_location.text! = "Location:-" + location
                }
                let file_url = bill_data["file_url"] as? String
                if file_url != ""{
                    self.my_url = URL(string:file_url!)
                    let myRequest = URLRequest(url: self.my_url!)
                    self.mwebView.load(myRequest)
                }

            }
        }

    }
    
    @objc func barButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func arroveBtn_Action(sender:UIButton){
        if Reachability.isConnectedToNetwork() == true{
        updateBillApi(bill_status: "1")
        }else{
            KAppDelegate.showAlertNotification(checkInternet)
        }
    }
    @IBAction func disarroveBtn_Action(sender:UIButton){
        if Reachability.isConnectedToNetwork() == true{
        updateBillApi(bill_status: "2")
        }else{
            KAppDelegate.showAlertNotification(checkInternet)
        }

    }

    
    func updateBillApi(bill_status:String){
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let id = alldetails["id"] as! String
        
        let urlStr = baseUrl + update_bill
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"id":id,"bill_status":bill_status] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
            if succ{
                self.navigationController?.popViewController(animated: true)
                KAppDelegate.showAlertNotification(result["message"] as! String)

            }
        }

    }
}
