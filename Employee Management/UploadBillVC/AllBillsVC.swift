//
//  AllBillsVC.swift
//  Employee Management
//
//  Created by vidya on 28/10/2020.
//

import UIKit

class AllBillsVC: UIViewController {
    @IBOutlet weak var allbillTbl:UITableView!
    var allbillList = [[String:Any]]()
    var menuType:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "All Bills"
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
            item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
        // Do any additional setup after loading the view.
    }
    @objc func barButtonAction(){
        if self.menuType == "menu"{
            KAppDelegate.makeRootViewController()
        }else{
        self.navigationController?.popViewController(animated: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        if Reachability.isConnectedToNetwork() == true{
           getBillApiCall()
        }else{
            KAppDelegate.showAlertNotification(checkInternet)
        }

    }
    
    func getBillApiCall(){
            self.allbillList.removeAll()
            let userid = UserDefaults.standard.object(forKey: "user_id")
            let accesToken = UserDefaults.standard.object(forKey: "access_token")
            let urlStr = baseUrl + get_my_uploaded_bills
            let leaveDetials = ["user_id":userid!,"access_token":accesToken!] as [String:Any]
            postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
                print("Result--\(result)")
                if succ{
                    let data = result["data"] as? [String:Any]
                    let bill_data = data!["bill_data"] as! [[String:Any]]
                    for dic in bill_data{
                        self.allbillList.append(dic)
                    }
                }
                self.allbillTbl.reloadData()
            }

        }
    @IBAction func createBtn_Action(sender:UIButton){
        let objvc = self.storyboard?.instantiateViewController(withIdentifier: "UploadBillsVC") as? UploadBillsVC
        self.navigationController?.pushViewController(objvc!, animated: true)

    }

}
extension AllBillsVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return allbillList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllBillsCell", for: indexPath) as! AllBillsCell
        let item = allbillList[indexPath.row]
        if let client_project_name = item["client_project_name"] as? String{
            cell.projectNameLbl.text! = client_project_name
        }
        if let bill_date = item["bill_date"] as? String{
            cell.dateLbl.text! = bill_date
        }
        if let bill_amount = item["bill_amount"] as? String{
            cell.billAmountLbl.text! = bill_amount
        }
        if let bill_status = item["bill_status"] as? String{
            if bill_status == "1"{
                cell.statusLbl.text! = "Approved"
                cell.statusLbl.textColor! = .green
            }else if bill_status == "2"{
                cell.statusLbl.text! = "Disapproved"
                cell.statusLbl.textColor! = .red
            }else{
                cell.statusLbl.text! = "Status"

            }
        }
        cell.selectionStyle = .none

       return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = allbillList[indexPath.row]
        if UserDefaults.standard.bool(forKey: "is_Admin") == true{

        let objvc = self.storyboard?.instantiateViewController(withIdentifier: "BillDetailVC") as? BillDetailVC
        objvc?.file_url = item["file_url"] as? String
        objvc?.alldetails = item
        self.navigationController?.pushViewController(objvc!, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let item = self.allbillList[indexPath.row]
            print("delete",item["id"] as! String)
            deleteLeveApiCall(id: item["id"] as! String)
            allbillList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            
        }
    }
    func deleteLeveApiCall(id:String){
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr = baseUrl + delete_bill
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"id":id] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                self.getBillApiCall()
                KAppDelegate.showAlertNotification(result["message"] as! String)

            }
        }
    }


}

