//
//  LoginVC.swift
//  Employee Management
//
//  Created by vidya on 12/10/2020.
//

import UIKit
import SlideMenuControllerSwift

class LoginVC: UIViewController ,UITextFieldDelegate{
    @IBOutlet weak var usernametxt:UITextField!
    @IBOutlet weak var passwordtxt:UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func loginBtn_Action(Sender:AnyObject){
        if self.usernametxt.text!.isEmpty != true{
            if self.passwordtxt.text!.isEmpty != true{
                if Reachability.isConnectedToNetwork() == true{
                loginApiCall()
                }else{
                    KAppDelegate.showAlertNotification(checkInternet)
                }
            }else{
                KAppDelegate.showAlertNotification("Eneter password")
            }
        }else{
            KAppDelegate.showAlertNotification("Eneter Email.")
        }
    }
    func loginApiCall(){

        //let UUID = NSUUID().uuidString

        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        print("deviceID--\(deviceID)")//"52C687E7-5D86-42B8-B7E3-454B6E70A813"
        let urlStr = baseUrl + login
        let userloginDetials = ["email":self.usernametxt.text!,"password":self.passwordtxt.text!,"device_type":"IOS","last_login_imei":deviceID] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: userloginDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                let data = result["data"] as? [String:Any]
                let user_data = data!["user_data"] as? [String:Any]
                let access_token = user_data!["access_token"] as? String
                let user_id = user_data!["user_id"] as? String
                let is_admin = user_data!["is_admin"] as? String
                let full_name = user_data!["full_name"] as? String
                let email = user_data!["email"] as? String
                let profile_picture = user_data!["profile_picture"] as? String
                if is_admin == "true"{
                    print("Admin")
                    UserDefaults.standard.setValue(true, forKey: "is_Admin")

                }
                UserDefaults.standard.setValue(true, forKey: "isLogin")
                UserDefaults.standard.setValue(user_id!, forKey: "user_id")
                UserDefaults.standard.setValue(access_token!, forKey: "access_token")
                UserDefaults.standard.setValue(full_name!, forKey: "full_name")
                UserDefaults.standard.setValue(email!, forKey: "email")
                UserDefaults.standard.setValue(profile_picture!, forKey: "profile_picture")
                if let emp_id = user_data!["emp_id"] as? String{
                    UserDefaults.standard.setValue(emp_id, forKey: "emp_id")
                }
                if let aadhaar_number = user_data!["aadhaar_number"] as? String{
                    UserDefaults.standard.setValue(aadhaar_number, forKey: "aadhaar_number")
                }
                if let address = user_data!["address"] as? String{
                    UserDefaults.standard.setValue(address, forKey: "address")
                }
                if let date_of_birth = user_data!["date_of_birth"] as? String{
                    UserDefaults.standard.setValue(date_of_birth, forKey: "date_of_birth")
                }
                if let designation = user_data!["designation"] as? String{
                    UserDefaults.standard.setValue(designation, forKey: "designation")
                }
                if let esic_number = user_data!["esic_number"] as? String{
                    UserDefaults.standard.setValue(esic_number, forKey: "esic_number")
                }
//                if let full_name = user_data!["full_name"] as? String{
//                    UserDefaults.standard.setValue(full_name, forKey: "full_name")
//                }
                if let pan_number = user_data!["pan_number"] as? String{
                    UserDefaults.standard.setValue(pan_number, forKey: "pan_number")
                }
                
                if let phone_number = user_data!["phone_number"] as? String{
                    UserDefaults.standard.setValue(phone_number, forKey: "phone_number")
                }
                if let uan_number = user_data!["uan_number"] as? String{
                    UserDefaults.standard.setValue(uan_number, forKey: "uan_number")
                }
                if let joining_date = user_data!["joining_date"] as? String{
                    UserDefaults.standard.setValue(joining_date, forKey: "joining_date")
                }
                self.makeRootViewController()
            }
       }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        animateViewMoving(true, moveValue: 100)

    }
    func animateViewMoving (_ up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()

    }
    func textFieldDidEndEditing(_ textField: UITextField) {
     animateViewMoving(false, moveValue: 100)
      textField.resignFirstResponder()

    }

    @IBAction func forgotBtn_Action(Sender:AnyObject){
        let objvc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotVC") as? ForgotVC
        self.navigationController?.pushViewController(objvc!, animated: true)

    }

    func makeRootViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
         let customTabbarViewController = storyboard.instantiateViewController(withIdentifier: "CustomTabBarVC") as? CustomTabBarVC
            let leftMenuViewController = storyboard.instantiateViewController(withIdentifier: "MenuVC") as? MenuVC
        let rightMenuController = storyboard.instantiateViewController(withIdentifier: "RightMenuVC") as? RightMenuVC

        let navigationViewController: UINavigationController = UINavigationController(rootViewController: customTabbarViewController!)
        
        //Create Side Menu View Controller with main, left and right view controller
        let sideMenuViewController = SlideMenuController(mainViewController: navigationViewController, leftMenuViewController: leftMenuViewController!, rightMenuViewController: rightMenuController!)
        self.navigationController?.pushViewController(sideMenuViewController, animated: true)

//        window?.rootViewController = sideMenuViewController
//        window?.makeKeyAndVisible()
    }

}

extension UIDevice {
   var modelName: String {
       var systemInfo = utsname()
       uname(&systemInfo)
       let machineMirror = Mirror(reflecting: systemInfo.machine)
       let identifier = machineMirror.children.reduce("") { identifier, element in
           guard let value = element.value as? Int8, value != 0 else { return identifier }
           return identifier + String(UnicodeScalar(UInt8(value)))
       }
       return identifier
   }
}
