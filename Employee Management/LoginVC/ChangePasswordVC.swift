//
//  ChangePasswordVC.swift
//  Employee Management
//
//  Created by vidya on 06/11/2020.
//

import UIKit

class ChangePasswordVC: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var otpTxt:UITextField!
    @IBOutlet weak var newpasswordTxt:UITextField!
    @IBOutlet weak var confirmpasswordTxt:UITextField!
    @IBOutlet weak var passwordBtn:UIButton!
    @IBOutlet weak var confirmBtn:UIButton!
    var menuType:String!
    var emailStr:String! = ""
    var iconClick = true

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Verify OTP"
        self.navigationItem.hidesBackButton = true
        
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
        item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
print("emailStr--\(emailStr)")
        // Do any additional setup after loading the view.
    }
    @objc func barButtonAction() {
        if self.menuType == "menu"{
            KAppDelegate.makeRootViewController()
        }else{
        self.navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func allBtn_Action(sender:UIButton){
        switch sender.tag {
        case 100:
            print("resend")
        case 101:
            if(iconClick == true) {
                passwordBtn.setImage(UIImage(named: "eye-slash"), for: .normal)
                newpasswordTxt.isSecureTextEntry = true
            } else {
                passwordBtn.setImage(UIImage(named: "eye"), for: .normal)
                newpasswordTxt.isSecureTextEntry = false
            }
            iconClick = !iconClick

            print("newpass")
        case 102:
            print("confirmpass")
            if(iconClick == true) {
                confirmBtn.setImage(UIImage(named: "eye-slash"), for: .normal)
                confirmpasswordTxt.isSecureTextEntry = true
            } else {
                confirmBtn.setImage(UIImage(named: "eye"), for: .normal)

                confirmpasswordTxt.isSecureTextEntry = false
            }
            iconClick = !iconClick


        default:
            break
        }

    }

    @IBAction func okBtn(sender:UIButton){
        if self.otpTxt.text!.isEmpty != true{
            if self.newpasswordTxt.text!.isEmpty != true{
                if self.confirmpasswordTxt.text!.isEmpty != true{
                    forgetApi()
                }else{
                    KAppDelegate.showAlertNotification("Enter Confirm Password")

                }
            }else{
                KAppDelegate.showAlertNotification("Enter Password")

            }
        }else{
            KAppDelegate.showAlertNotification("Enter OTP")
        }
        
        
    }
    
    func forgetApi(){
        let urlStr = baseUrl + reset_password
        let userloginDetials = ["email":emailStr!,"otp":self.otpTxt.text!,"password":self.newpasswordTxt.text!,"c_password":self.confirmpasswordTxt.text!] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: userloginDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                self.navigationController?.popViewController(animated: true)
                KAppDelegate.showAlertNotification(result["message"] as!  String)
            }
        }

    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
