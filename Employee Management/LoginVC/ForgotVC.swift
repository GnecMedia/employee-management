//
//  ForgotVC.swift
//  Employee Management
//
//  Created by vidya on 12/10/2020.
//

import UIKit

class ForgotVC: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var emailtxt:UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Forgot Password"
        self.navigationItem.hidesBackButton = true
        
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
        item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item


        // Do any additional setup after loading the view.
    }
    @objc func barButtonAction() {
        self.navigationController?.popViewController(animated: true)

    }

    @IBAction func submitBtnAction(sender:UIButton){
        if self.emailtxt.text!.isEmpty != true{
            if Reachability.isConnectedToNetwork() == true{
            forgetApi()
            }else{
                KAppDelegate.showAlertNotification(checkInternet)
            }
        }else{
            KAppDelegate.showAlertNotification("Please Enter Email")
        }
    }
    
    func forgetApi(){
        let urlStr = baseUrl + forgot_password
        let userloginDetials = ["email":self.emailtxt.text!] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: userloginDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                let objvc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as? ChangePasswordVC
                objvc!.emailStr = self.emailtxt.text!
                self.navigationController?.pushViewController(objvc!, animated: true)
                KAppDelegate.showAlertNotification(result["message"] as!  String)

            }
        }

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
