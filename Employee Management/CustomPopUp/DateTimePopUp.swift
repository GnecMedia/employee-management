//
//  DateTimePopUp.swift
//  Employee Management
//
//  Created by vidya on 28/10/2020.
//

import UIKit
protocol DatePickerPopUpViewDelegate {
    func didPresDoneActionBtn(_ controller: DateTimePopUp,isDate:String)
    func didPresDoneTimeActionBtn(_ controller: DateTimePopUp,isTime:String)
    func didPresEndDateActionBtn(_ controller: DateTimePopUp,isendDate:String)
    func didPresEndTimeActionBtn(_ controller: DateTimePopUp,isendTime:String)

}


class DateTimePopUp: UIViewController {
    @IBOutlet weak var datePickerView: UIDatePicker!
    var delegate: DatePickerPopUpViewDelegate?  = nil
    var dateMode:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        if dateMode == "time" || dateMode == "end_Time"{
            self.datePickerView.datePickerMode = .time
        }
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        self.view.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)

        // Do any additional setup after loading the view.
    }
    @IBAction func cancelBtnAction(sender:UIButton){
        removeAnimate()
    }

    @IBAction func doneBtnAction(sender:UIButton){
        if dateMode == "time"{
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = DateFormatter.Style.short
            dateFormatter.dateFormat = "hh:mm" //"hh:mm a"
            let timeDate = dateFormatter.string(from: datePickerView.date)
            self.delegate?.didPresDoneTimeActionBtn(self, isTime: timeDate)
        }else if dateMode == "end_Time"{
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = DateFormatter.Style.short
            dateFormatter.dateFormat = "hh:mm" //"hh:mm a"
            let timeDate = dateFormatter.string(from: datePickerView.date)
            self.delegate?.didPresEndTimeActionBtn(self, isendTime: timeDate)

        }else if dateMode == "end_Date"{
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.short
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let strDate = dateFormatter.string(from: datePickerView.date)
            self.delegate?.didPresEndDateActionBtn(self, isendDate: strDate)

        }else{
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strDate = dateFormatter.string(from: datePickerView.date)
        print("Date--\(strDate)")
            self.delegate?.didPresDoneActionBtn(self, isDate: strDate)
        }
        removeAnimate()
    }

    internal func showInView(_ aView: UIView!, animated: Bool)
    {
        aView.addSubview(self.view)
        
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    @objc func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
