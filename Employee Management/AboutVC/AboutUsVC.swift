//
//  AboutUsVC.swift
//  Employee Management
//
//  Created by vidya on 02/11/2020.
//

import UIKit
import WebKit
class AboutUsVC: UIViewController,WKUIDelegate {
    @IBOutlet weak var webView:WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "ABOUT US"
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
            item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
        if Reachability.isConnectedToNetwork() == true{
        let myURL = URL(string:"https://www.gnecmedia.com/")
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
        }else{
            KAppDelegate.showAlertNotification(checkInternet)
        }

        // Do any additional setup after loading the view.
    }
    
    @objc func barButtonAction(){
        KAppDelegate.makeRootViewController()

       // self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
