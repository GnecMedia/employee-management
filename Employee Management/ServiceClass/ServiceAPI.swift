//
//  ServiceAPI.swift
//  Employee Management
//
//  Created by vidya on 04/11/2020.
//
import UIKit
import Foundation
import Alamofire
import RappleProgressHUD


/*func remotePostServiceWithoutHeader(_ urlString : String! , params : Dictionary<String, Any> , callback:@escaping (_ data: Dictionary<String, AnyObject>?, _ error: NSError? ) -> Void)  {
    let urlString =  "\(baseConstants.baseUrl)\(urlString!)"
  // print("url--\(urlString) \(params)")
    var request = URLRequest(url: NSURL(string: urlString)! as URL)
    request.httpMethod = "POST"
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    
    let data = try! JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
    let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
    request.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
    Alamofire.request(request as URLRequestConvertible).responseJSON(){ response in
        guard response.result.error == nil else {
            print("ERROR")
            callback(nil , response.result.error! as NSError )
            return
        }
        if let value = response.result.value {
            if let result = value as? Dictionary<String, AnyObject> {
                print("Response for POST Data :\(urlString):\(value)")
                callback(result , nil )

            }else{
               // KAppDelegate.showNotification(InvalidJson)
                return
            }
        }
    }
}*/
//MARK:- Get Alamofire Method
 func getAPi(urlStr:String, callback:@escaping (( _ success :Bool, _ results: [String:Any])->())){
       let apiurl = URLComponents(string:  urlStr)
    //let header = "Bearer \(baseConstants.kUserDefaults.object(forKey: "AuthToken")!)"
    let headers = ["Content-Type":"application/json","Authorization":""]
      print("URL-\(apiurl!) ")
       print("getUrl==",apiurl!)
       Alamofire.request(apiurl!,method: .get, encoding: JSONEncoding.default,headers:headers).responseJSON { (respose) in
       if let result = respose.result.value as? [String :Any] {
               print(result)
          //RappleActivityIndicatorView.stopAnimation()

                  callback(true,result)
              } else {
                RappleActivityIndicatorView.stopAnimation()
                  print("error==",respose.result.error!)
                  callback(false,[:])
              }

        return
       }
   }
//MARK:-- Post With Header
func postAPiwithOut1(urlStr:String,parameter:[String:Any],callback:@escaping (( _ success :Bool,_ errorcode:Int, _ results: [String:Any])->())){
      let apiurl = URLComponents(string:  urlStr)
    let username = "auth@gnecmedia.com"
    let password = "sRpX])3jt7a{Rd(f"
    let loginString = String(format: "%@:%@", username, password)
    let loginData = loginString.data(using: String.Encoding.utf8)!
    let base64LoginString = loginData.base64EncodedString()
    print("base64LoginString--\(base64LoginString)")
   let headers = ["Content-Type":"application/json","Authorization":"Basic \(base64LoginString)"]
      print("getUrl==",apiurl!)
     print("parameter==",parameter)
    print("headers==",headers)
    RappleActivityIndicatorView.startAnimating()

   Alamofire.request(apiurl!,method: .post,parameters: parameter,encoding:
      JSONEncoding.default,headers:headers).responseJSON { (respose) in
       if let result = respose.result.value as? [String :Any] {
        print(result)
        let errorCode = respose.response!.statusCode
         print("errorCode--\(errorCode)")
        let status = "\((result as AnyObject).value(forKey: "success")!)"
        if status == "1"{
            RappleActivityIndicatorView.stopAnimation()
            callback(true,errorCode,result)
        }else{
           RappleActivityIndicatorView.stopAnimation()
            let mess = "\((result as AnyObject).value(forKey: "message")!)"
            KAppDelegate.showAlertNotification(mess)
            callback(true,errorCode, result)
        }

       } else {
           print("error==",respose.result.error!)
        RappleActivityIndicatorView.stopAnimation()

        callback(false,0,[:])
       }
          return
      }
  }

func postAPiwithOut(urlStr:String,parameter:[String:Any],callback:@escaping (( _ success :Bool, _ results: [String:Any])->())){
      let apiurl = URLComponents(string:  urlStr)
    let username = "auth@gnecmedia.com"
    let password = "sRpX])3jt7a{Rd(f"
    let loginString = String(format: "%@:%@", username, password)
    let loginData = loginString.data(using: String.Encoding.utf8)!
    let base64LoginString = loginData.base64EncodedString()
    print("base64LoginString--\(base64LoginString)")
   let headers = ["Content-Type":"application/json","Authorization":"Basic \(base64LoginString)"]
      print("getUrl==",apiurl!)
     print("parameter==",parameter)
    print("headers==",headers)
    RappleActivityIndicatorView.startAnimating()

   Alamofire.request(apiurl!,method: .post,parameters: parameter,encoding:
      JSONEncoding.default,headers:headers).responseJSON { (respose) in
       if let result = respose.result.value as? [String :Any] {
        print(result)
        let status = "\((result as AnyObject).value(forKey: "success")!)"
        if status == "1"{
            RappleActivityIndicatorView.stopAnimation()
            callback(true,result)
        }else{
           RappleActivityIndicatorView.stopAnimation()
            let mess = "\((result as AnyObject).value(forKey: "message")!)"
            KAppDelegate.showAlertNotification(mess)
            callback(false, result)
        }

       } else {
           print("error==",respose.result.error!)
        RappleActivityIndicatorView.stopAnimation()

           callback(false,[:])
       }
          return
      }
  }

//MARK:- Post Alamofire Method
 func postAPi(urlStr:String,parameter:[String:Any],callback:@escaping (( _ success :Bool, _ results: [String:Any])->())){
       let apiurl = URLComponents(string:  urlStr)
  //  let header = "Bearer \(baseConstants.kUserDefaults.object(forKey: "AuthToken")!)"
    let headers = ["Content-Type":"application/json","Authorization":""]
       print("getJobsUrl==",apiurl!)
      print("parameter==",parameter)
    print("headers==",headers)

    Alamofire.request(apiurl!,method: .post,parameters: parameter,encoding:
       JSONEncoding.default,headers:headers).responseJSON { (respose) in
        if let result = respose.result.value as? [String :Any] {
         print(result)
            let status = "\((result as AnyObject).value(forKey: "success")!)"
            if status == "1"{
                callback(true,result)
            }else{
               // RappleActivityIndicatorView.stopAnimation()
                let mess = "\((result as AnyObject).value(forKey: "message")!)"
                KAppDelegate.showAlertNotification(mess)
                callback(false, result)
            }
        } else {
            print("error==",respose.result.error!)
            RappleActivityIndicatorView.stopAnimation()

            callback(false,[:])
        }
           return
       }
   }

func createMultipartUploadData(urlStr:String , parameter :[String:Any],selectedevent:UIImage!,callback:@escaping((_ success:Bool, _ result: [String:Any])->())) {
    let apiurl = URLComponents(string:  urlStr)
  //  let header = "Bearer \(baseConstants.kUserDefaults.object(forKey: "AuthToken")!)"
    let headers = ["Authorization":""]
    print(parameter)
    print(apiurl!)
    print("headers--\(headers)")

    Alamofire.upload(multipartFormData: { (multipartFormData) in
        for (key, value) in parameter {
            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
        }
            if selectedevent.size != .zero {
            print("image--\(selectedevent!)")
            let imageData = selectedevent.jpegData(compressionQuality: 0.5)
            multipartFormData.append(imageData!, withName: "image", fileName: "image.jpg", mimeType: "image/jpg")
        }

    }, usingThreshold: UInt64.init(), to: apiurl!, method: .post,headers:headers) { (result) in
        switch result{
        case .success(let upload, _, _):
            upload.responseJSON { response in
                print("response json---",response)
                if let result = response.result.value as? [String :Any] {
                    let status = "\((result as AnyObject).value(forKey: "success")!)"
                    if status == "1"{
                        callback(true,result)
                    }else{
                       // RappleActivityIndicatorView.stopAnimation()
                        let mess = "\((result as AnyObject).value(forKey: "message")!)"
                        KAppDelegate.showAlertNotification(mess)
                        callback(false, result)
                    }
                    }else{
                        RappleActivityIndicatorView.stopAnimation()
                       // KAppDelegate.showNotification(InvalidJson)
                        return
                    }
                }
            case .failure(let error):
            print("Error in upload: \(error.localizedDescription)")
            //KAppDelegate.showNotification("\(error)")
            return
        }
    }
}

func uploadBillImage(urlStr:String , parameter :[String:Any],profile:[UIImage],callback:@escaping((_ success:Bool, _ result: [String:Any])->())) {
    let apiurl = URLComponents(string:  urlStr)
    let username = "auth@gnecmedia.com"
    let password = "sRpX])3jt7a{Rd(f"
    let loginString = String(format: "%@:%@", username, password)
    let loginData = loginString.data(using: String.Encoding.utf8)!
    let base64LoginString = loginData.base64EncodedString()
   let headers = ["Authorization":"Basic \(base64LoginString)"]
    print(parameter)
    print(apiurl!)
    print("headers--\(headers)")
   Alamofire.upload(multipartFormData: { (multipartFormData) in
              for (key, value) in parameter {
                  if value is String || value is Int {
                      multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                  }
              }
               var i = Int()
              for image in profile{
                  if let imageData = image.jpegData(compressionQuality: 0.2) {
                      multipartFormData.append(imageData, withName: "file", fileName: "file.jpeg", mimeType: "file/jpeg")
                  }
               i = i + 1
              }
          }, to: apiurl!,
             method: .post,
             headers: headers) { (result) in
              print("result",result)
              switch result {
              case .success(let upload, _, _):
                  upload.uploadProgress(closure: { (progress) in
                      print("Upload Progress: \(progress.fractionCompleted)")
                  })
                  upload.responseJSON { response in
                      print("response json",response)
                    if let result = response.result.value as? [String :Any] {
                    callback(true,result)
                    }
                     return
                  }
              case .failure(let encodingError):
                  print("Upload error:",encodingError.localizedDescription)
//                  callback(false,encodingError.localizedDescription)
                  return
              }
          }
}

func postBillUploadData(urlStr:String ,fileType:String, parameter :[String:Any],selectedevent:UIImage!,callback:@escaping((_ success:Bool, _ result: [String:Any])->())) {
    let apiurl = URLComponents(string:  urlStr)
    let username = "auth@gnecmedia.com"
    let password = "sRpX])3jt7a{Rd(f"
    let loginString = String(format: "%@:%@", username, password)
    let loginData = loginString.data(using: String.Encoding.utf8)!
    let base64LoginString = loginData.base64EncodedString()
   let headers = ["Authorization":"Basic \(base64LoginString)"]
    print(parameter)
    print(apiurl!)
    print("headers--\(headers)")
    RappleActivityIndicatorView.startAnimating()

    Alamofire.upload(multipartFormData: { (multipartFormData) in
        for (key, value) in parameter {
            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)

        }
        if fileType == "pdf"{
        print("pdfData--\(pdfData!)")
        multipartFormData.append(pdfData! as Data, withName: "file", fileName: "test.pdf", mimeType:"file/pdf")
        }else{
          if selectedevent != nil{
            print("image--\(selectedevent!)")
            let imageData = selectedevent.jpegData(compressionQuality: 0.5)
            multipartFormData.append(imageData!, withName: "file", fileName: "file.jpg", mimeType: "file/jpeg")
        }
        }

    }, usingThreshold: UInt64.init(), to: apiurl!, method: .post,headers:headers) { (result) in
        switch result{
        case .success(let upload, _, _):
            upload.responseJSON { response in
                print("response json---",response)
                if let result = response.result.value as? [String :Any] {
                    let status = "\((result as AnyObject).value(forKey: "success")!)"
                    if status == "1"{
                        RappleActivityIndicatorView.stopAnimation()
                        callback(true,result)
                        
                    }else{
                        RappleActivityIndicatorView.stopAnimation()
                        let mess = "\((result as AnyObject).value(forKey: "message")!)"
                        KAppDelegate.showAlertNotification(mess)
                        callback(false, result)
                    }
                    }else{
                        RappleActivityIndicatorView.stopAnimation()
                       // KAppDelegate.showNotification(InvalidJson)
                        return
                    }
                }
            
        
        case .failure(let error):
            print("Error in upload: \(error.localizedDescription)")
            //KAppDelegate.showNotification("\(error)")
            return
        }
    }
}


func postadminProfileData(urlStr:String , parameter :[String:Any],selectedevent:UIImage!,callback:@escaping((_ success:Bool, _ result: [String:Any])->())) {
    let apiurl = URLComponents(string:  urlStr)
    let username = "auth@gnecmedia.com"
    let password = "sRpX])3jt7a{Rd(f"
    let loginString = String(format: "%@:%@", username, password)
    let loginData = loginString.data(using: String.Encoding.utf8)!
    let base64LoginString = loginData.base64EncodedString()
   let headers = ["Authorization":"Basic \(base64LoginString)"]
    print(parameter)
    print(apiurl!)
    print("headers--\(headers)")
    RappleActivityIndicatorView.startAnimating()
    Alamofire.upload(multipartFormData: { (multipartFormData) in
        for (key, value) in parameter {
            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)

        }
            if selectedevent.size != .zero {
            print("image--\(selectedevent!)")
            let imageData = selectedevent.jpegData(compressionQuality: 0.5)
            multipartFormData.append(imageData!, withName: "profile_picture", fileName: "profile_picture.jpg", mimeType: "profile_picture/jpg")
        }

    }, usingThreshold: UInt64.init(), to: apiurl!, method: .post,headers:headers) { (result) in
        switch result{
        case .success(let upload, _, _):
            upload.responseJSON { response in
                print("response json---",response)
                if let result = response.result.value as? [String :Any] {
                    let status = "\((result as AnyObject).value(forKey: "success")!)"
                    if status == "1"{
                        RappleActivityIndicatorView.stopAnimation()
                        callback(true,result)
                        
                    }else{
                        RappleActivityIndicatorView.stopAnimation()
                        let mess = "\((result as AnyObject).value(forKey: "message")!)"
                        KAppDelegate.showAlertNotification(mess)
                        callback(false, result)
                    }
                    }else{
                        RappleActivityIndicatorView.stopAnimation()

                       // KAppDelegate.showNotification(InvalidJson)
                        return
                    }
                }
            
        
        case .failure(let error):
            print("Error in upload: \(error.localizedDescription)")
            //KAppDelegate.showNotification("\(error)")
            return
        }
    }
}
