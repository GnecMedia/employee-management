//
//  AllLeaveVC.swift
//  Employee Management
//
//  Created by vidya on 28/10/2020.
//

import UIKit

class AllLeaveVC: UIViewController {
    @IBOutlet weak var allLeaveTbl:UITableView!
    var allleaveList = [[String:Any]]()
    var menuType:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "All Leave"
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
            item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
        // Do any additional setup after loading the view.
    }
    @objc func barButtonAction(){
        if self.menuType == "menu"{
            KAppDelegate.makeRootViewController()
        }else{
        self.navigationController?.popViewController(animated: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        allLeaveApiCall()

    }
    @IBAction func appleLeaveBtn(sender:UIButton){
        let objvc = self.storyboard?.instantiateViewController(withIdentifier: "ApplyLeaveVC") as? ApplyLeaveVC
        self.navigationController?.pushViewController(objvc!, animated: true)
        
    }
    func allLeaveApiCall(){
        self.allleaveList.removeAll()
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr = baseUrl + get_my_leave_application
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"year_month":""] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                let data = result["data"] as? [String:Any]
                let leave = data!["leave_data"] as! [[String:Any]]
                for dic in leave{
                    self.allleaveList.append(dic)
                }
            }
            self.allLeaveTbl.reloadData()
        }

    }

}
extension AllLeaveVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return allleaveList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllLeaveCell", for: indexPath) as! AllLeaveCell
        cell.selectionStyle = .none
        let item = allleaveList[indexPath.row]
        if let full_name = item["reason_of_leave"] as? String{
            cell.nameLbl.text! = full_name
        }
        if let start_date = item["start_date"] as? String{
            cell.dateLbl.text! = start_date
        }
//        if let start_time = item["start_time"] as? String{
//            cell.timelbl.text! = start_time
//        }
        if let leave_type = item["leave_type"] as? String{
            cell.leaveTypeLbl.text! = leave_type
        }
        if let reporting_manager = item["reporting_manager"] as? String{
            cell.remarkLbl.text! = reporting_manager
        }

       return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.allleaveList[indexPath.row]

        let objvc = self.storyboard?.instantiateViewController(withIdentifier: "ApplyLeaveVC") as? ApplyLeaveVC
        objvc?.id = item["id"] as? String
        objvc?.updateLeave = "leaveupdate"
        self.navigationController?.pushViewController(objvc!, animated: true)

    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let item = self.allleaveList[indexPath.row]
            //print("delete",item["id"] as! String)
            deleteLeveApiCall(id: item["id"] as! String)
          //  allleaveList.remove(at: indexPath.row)
          //  tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            
        }
    }
    func deleteLeveApiCall(id:String){
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr = baseUrl + delete_leave
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"id":id] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                self.allLeaveApiCall()
                KAppDelegate.showAlertNotification(result["message"] as! String)
            }
        }
    }

}
