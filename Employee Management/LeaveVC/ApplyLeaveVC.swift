//
//  ApplyLeaveVC.swift
//  Employee Management
//
//  Created by vidya on 28/10/2020.
//

import UIKit
import DropDown

class ApplyLeaveVC: UIViewController,UITextFieldDelegate,DatePickerPopUpViewDelegate {
    
    fileprivate var popViewController :  DateTimePopUp!
    @IBOutlet weak var startdatelbl:UILabel!
    @IBOutlet weak var enddatelbl:UILabel!
    @IBOutlet weak var starttimelbl:UILabel!
    @IBOutlet weak var endtimelbl:UILabel!
    @IBOutlet weak var remarktxt:UITextField!
    @IBOutlet weak var reportingtxt:UITextField!
    @IBOutlet weak var leaveBtn:UIButton!
    @IBOutlet weak var leaveUpdateBtn:UIButton!

    let dropDown = DropDown()
    @IBOutlet weak var timeconstant:NSLayoutConstraint!
    @IBOutlet weak var timeinputconstant:NSLayoutConstraint!
    var leave_type:String! = ""
    var menuType:String!
    var id:String! = ""
    var updateLeave:String! = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "APPLY LEAVE"
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
            item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
        self.timeconstant.constant = 0
        self.timeinputconstant.constant = 0

        showdropdown()
        if updateLeave == "leaveupdate"{
            self.leaveUpdateBtn.setTitle("UPDATE", for: .normal)
            get_leave_by_idApiCall()

        }
        // Do any additional setup after loading the view.
    }
    
    @objc func barButtonAction(){
        if self.menuType == "menu"{
            KAppDelegate.makeRootViewController()
        }else{
            self.navigationController?.popViewController(animated: true)

        }
    }
    func get_leave_by_idApiCall(){
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr = baseUrl + get_leave_by_id
        let leave_id_Detials = ["user_id":userid!,"access_token":accesToken!,"id":id!] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: leave_id_Detials){(succ, result) in
            print("Result--\(result)")
            if succ{
                let data = result["data"] as! [String:Any]
                let dict = data["leave_data"] as! [String:Any]
                let leave_type = dict["leave_type"] as! String
                self.leave_type = "\(leave_type)"
                self.leaveBtn.setTitle("\(leave_type)", for: .normal)

                if self.leave_type == "HALF_DAY"{
                    self.timeconstant.constant = 20
                    self.timeinputconstant.constant = 50
                }
                let reason_of_leave = dict["reason_of_leave"] as? String
                self.remarktxt.text! = reason_of_leave!
                let reporting_manager = dict["reporting_manager"] as? String
                self.reportingtxt.text! = reporting_manager!
                let start_date = dict["start_date"] as? String
                self.startdatelbl.text! = start_date!
                let end_date = dict["end_date"] as? String
                self.enddatelbl.text! = end_date!
                let end_time = dict["end_time"] as? String
                self.endtimelbl.text! = end_time!
                let start_time = dict["start_time"] as? String
                self.starttimelbl.text! = start_time!


            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    @IBAction func all_Date_Time_BtnAction(sender:AnyObject){
        switch sender.tag {
        case 100:
            let bundle = Bundle(for: DateTimePopUp.self)
            self.popViewController = DateTimePopUp(nibName: "DateTimePopUp", bundle: bundle)
          self.popViewController.delegate = self
            self.popViewController.showInView(self.view,animated: true)
            break
        case 101:
              let bundle = Bundle(for: DateTimePopUp.self)
              self.popViewController = DateTimePopUp(nibName: "DateTimePopUp", bundle: bundle)
            self.popViewController.delegate = self
            self.popViewController.dateMode = "end_Date"
              self.popViewController.showInView(self.view,animated: true)

            break
        case 102:
            let bundle = Bundle(for: DateTimePopUp.self)
            self.popViewController = DateTimePopUp(nibName: "DateTimePopUp", bundle: bundle)
            self.popViewController.dateMode = "time"
            self.popViewController.delegate = self
            self.popViewController.showInView(self.view,animated: true)

            break
        case 103:
            let bundle = Bundle(for: DateTimePopUp.self)
            self.popViewController = DateTimePopUp(nibName: "DateTimePopUp", bundle: bundle)
            self.popViewController.dateMode = "end_Time"
            self.popViewController.delegate = self
            self.popViewController.showInView(self.view,animated: true)

            break

        default:
            break
        }
    }
   @IBAction func leaveBtn_Ation(sender:UIButton){
    dropDown.show()

    }
    func showdropdown(){
        dropDown.anchorView = leaveBtn
        dropDown.dataSource = ["FULL_DAY", "HALF_DAY"]

        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            if index == 0{
                self.timeconstant.constant = 0
                self.timeinputconstant.constant = 0

            }else{
                self.timeconstant.constant = 20
                self.timeinputconstant.constant = 50

            }
            self.leave_type = "\(item)"
            self.leaveBtn.setTitle("\(item)", for: .normal)
        }

    }

    func didPresDoneActionBtn(_ controller: DateTimePopUp, isDate: String) {
        self.startdatelbl.text = isDate
        
    }
    func didPresEndDateActionBtn(_ controller: DateTimePopUp, isendDate: String) {
        self.enddatelbl.text = isendDate
    }
    
    func didPresEndTimeActionBtn(_ controller: DateTimePopUp, isendTime: String) {
        self.endtimelbl.text = isendTime
    }

    
    func didPresDoneTimeActionBtn(_ controller: DateTimePopUp, isTime: String) {
        self.starttimelbl.text = isTime
    }

    @IBAction func addleaveBtn_Action(sender:UIButton){
        if self.leave_type != ""{
            if self.startdatelbl.text?.isEmpty != true{
                if self.enddatelbl.text?.isEmpty != true{
                    if self.remarktxt.text?.isEmpty != true{
                        if self.reportingtxt.text?.isEmpty != true{
                            leaveapplyApiCall()

                        }else{
                            KAppDelegate.showAlertNotification("Please Enter Reporting manager")
                        }
                    }else{
                        KAppDelegate.showAlertNotification("Please Enter Reason")
                    }
                }else{
                    KAppDelegate.showAlertNotification("Please select end Date")
                }
            }else{
                KAppDelegate.showAlertNotification("Please select Start Date")
            }
        }else{
            KAppDelegate.showAlertNotification("Please select Leave Type")
        }
    }
    func leaveapplyApiCall(){
        
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr:String!
        if updateLeave == "leaveupdate"{
            urlStr = baseUrl + update_leave

        }else{
         urlStr = baseUrl + apply_leave
        }
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"leave_type":self.leave_type!,"start_date":self.startdatelbl.text!,"end_date":self.enddatelbl.text!,"start_time":self.starttimelbl.text!,"end_time":self.endtimelbl.text!,"reason_of_leave":self.remarktxt.text!,"reporting_manager":self.reportingtxt.text!,"emp_id":"","id":id!] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                self.navigationController?.popViewController(animated: true)
                KAppDelegate.showAlertNotification(result["message"] as! String)

            }
        }


    }
}
