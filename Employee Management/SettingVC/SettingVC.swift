//
//  SettingVC.swift
//  Employee Management
//
//  Created by vidya on 19/10/2020.
//

import UIKit

class SettingVC: SideBaseViewController,UITextFieldDelegate {
    @IBOutlet weak var oldPasswordTxt:UITextField!
    @IBOutlet weak var newPasswordTxt:UITextField!
    @IBOutlet weak var confirmPasswordTxt:UITextField!
    @IBOutlet weak var oldBtn:UIButton!
    @IBOutlet weak var newBtn:UIButton!
    @IBOutlet weak var confirmBtn:UIButton!
    var iconClick = true

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.topItem?.title = "Settings"
        self.navigationController?.navigationBar.isHidden = false
        // Do any additional setup after loading the view.
    }
    @objc func barButtonAction() {
        self.navigationController?.popViewController(animated: true)

    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @IBAction func all_showPasswordBtn(sender:UIButton){
        switch sender.tag {
        case 100:
            if(iconClick == true) {
                oldBtn.setImage(UIImage(named: "eye-slash"), for: .normal)
                oldPasswordTxt.isSecureTextEntry = true
            } else {
                oldBtn.setImage(UIImage(named: "eye"), for: .normal)
                oldPasswordTxt.isSecureTextEntry = false
            }
            iconClick = !iconClick

        case 101:
            if(iconClick == true) {
                newBtn.setImage(UIImage(named: "eye-slash"), for: .normal)
                newPasswordTxt.isSecureTextEntry = true
            } else {
                newBtn.setImage(UIImage(named: "eye"), for: .normal)
                newPasswordTxt.isSecureTextEntry = false
            }
            iconClick = !iconClick

        case 102:
            if(iconClick == true) {
                confirmBtn.setImage(UIImage(named: "eye-slash"), for: .normal)
                confirmPasswordTxt.isSecureTextEntry = true
            } else {
                confirmBtn.setImage(UIImage(named: "eye"), for: .normal)
                confirmPasswordTxt.isSecureTextEntry = false
            }
            iconClick = !iconClick


        default:
            break
        }
    }
    @IBAction func updateBtn(sender:UIButton){
        if self.oldPasswordTxt.text?.isEmpty != true{
            if self.newPasswordTxt.text?.isEmpty != true{
                if self.confirmPasswordTxt.text?.isEmpty != true{
                    if Reachability.isConnectedToNetwork() == true{
                    changePasswordApicall()
                    }else{
                        KAppDelegate.showAlertNotification(checkInternet)
                    }
                }else{
                    KAppDelegate.showAlertNotification("Please Enter Confirm Password")
                }
            }else{
                KAppDelegate.showAlertNotification("Please Enter New Password")
            }
        }else{
            KAppDelegate.showAlertNotification("Please Enter old Password")
        }
        
    }
    
    func changePasswordApicall(){
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr = baseUrl + change_password
        let changeDetials = ["user_id":userid!,"access_token":accesToken!,"old_password":self.oldPasswordTxt.text!,"new_password":self.newPasswordTxt.text!,"confirm_password":self.confirmPasswordTxt.text!] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: changeDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                KAppDelegate.showAlertNotification(result["message"] as! String)
            }
        }

    }
}
