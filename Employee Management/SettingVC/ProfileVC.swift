//
//  ProfileVC.swift
//  Employee Management
//
//  Created by vidya on 19/10/2020.
//

import UIKit
import SDWebImage


class ProfileVC: SideBaseViewController {
    @IBOutlet weak var profilrTableview:UITableView!
    var profileList = [[String:Any]]()
    @IBOutlet weak var profileImg:UIImageView!
    @IBOutlet weak var userIdlbl:UILabel!
    @IBOutlet weak var emaillbl:UILabel!
    @IBOutlet weak var phonelbl:UILabel!
    @IBOutlet weak var doblbl:UILabel!
    @IBOutlet weak var dateofjoininglbl:UILabel!
    @IBOutlet weak var addresslbl:UILabel!
    @IBOutlet weak var adharnumberlbl:UILabel!
    @IBOutlet weak var pannumberlbl:UILabel!
    @IBOutlet weak var unanumberlbl:UILabel!
    @IBOutlet weak var esiclbl:UILabel!
    @IBOutlet weak var namelbl:UILabel!
    @IBOutlet weak var profilelbl:UILabel!
    var menuType:String!
    override func viewDidLoad() {
        super.viewDidLoad()
      //  self.title = "PROFILE"
        self.navigationController?.navigationBar.isHidden = false
        self.profileImg.layer.cornerRadius = self.profileImg.frame.height / 2
        if UserDefaults.standard.bool(forKey: "is_Admin") == true{
            if Reachability.isConnectedToNetwork() == true{
                empolyeeApi()
            }else{
                KAppDelegate.showAlertNotification(checkInternet)
            }
        }else{
            if let full_name = UserDefaults.standard.object(forKey: "full_name") as? String{
                self.namelbl.text! = full_name

            }

            if let email = UserDefaults.standard.object(forKey: "email") as? String{
                self.emaillbl.text! = email
            }
            if let profile_picture = UserDefaults.standard.object(forKey: "profile_picture") as? String{
            self.profileImg.sd_setImage(with: URL(string: profile_picture))
            }
            if let user_id = UserDefaults.standard.object(forKey: "user_id") as? String{
                self.userIdlbl.text! = user_id
            }
            if let designation = UserDefaults.standard.object(forKey: "designation") as? String{
            self.profilelbl.text! = designation
            }
            if let address = UserDefaults.standard.object(forKey: "address") as? String{
                self.addresslbl.text! = address
            }
            if let date_of_birth = UserDefaults.standard.object(forKey: "date_of_birth") as? String{
                self.doblbl.text! = date_of_birth
            }
            if let esic_number = UserDefaults.standard.object(forKey: "esic_number") as? String{
                self.esiclbl.text! = esic_number
            }
            if let pan_number = UserDefaults.standard.object(forKey: "pan_number") as? String{
                self.pannumberlbl.text! = pan_number
            }
            if let phone_number = UserDefaults.standard.object(forKey: "phone_number") as? String{
                self.phonelbl.text! = phone_number
            }
            if let uan_number = UserDefaults.standard.object(forKey: "uan_number") as? String{
                self.unanumberlbl.text! = uan_number
            }
            if let joining_date = UserDefaults.standard.object(forKey: "joining_date") as? String{
                self.dateofjoininglbl.text! = joining_date
            }
//            if let email = UserDefaults.standard.object(forKey: "email") as? String{
//                self.emaillbl.text! = email
//            }

        }
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func empolyeeApi(){
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr = baseUrl + get_employee_by_id
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"emp_id":userid!] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                let data = result["data"] as? [String:Any]
                let employee_data = data!["employee_data"] as! Dictionary<String,Any>
                if let emp_id = employee_data["emp_id"] as? String{
                    self.userIdlbl.text! = emp_id
                }
//                if let aadhaar_number = employee_data["aadhaar_number"] as? String{
//                    self.adharnumberlbl.text! = aadhaar_number
//                }
                if let address = employee_data["address"] as? String{
                    self.addresslbl.text! = address
                }
                if let date_of_birth = employee_data["date_of_birth"] as? String{
                    self.doblbl.text! = date_of_birth
                }
                if let designation = employee_data["designation"] as? String{
                    self.profilelbl.text! = designation
                }
                if let email = employee_data["email"] as? String{
                    self.emaillbl.text! = email
                }
                if let esic_number = employee_data["esic_number"] as? String{
                    self.esiclbl.text! = esic_number
                }
                if let full_name = employee_data["full_name"] as? String{
                    self.namelbl.text! = full_name
                }
                if let pan_number = employee_data["pan_number"] as? String{
                    self.pannumberlbl.text! = pan_number
                }
                
                if let phone_number = employee_data["phone_number"] as? String{
                    self.phonelbl.text! = phone_number
                }
                if let uan_number = employee_data["uan_number"] as? String{
                    self.unanumberlbl.text! = uan_number
                }
                if let profile_picture = employee_data["profile_picture"] as? String{
                    self.profileImg.sd_setImage(with: URL(string: profile_picture))
                }
                if let joining_date = employee_data["joining_date"] as? String{
                    self.dateofjoininglbl.text! = joining_date
                }
            }
        }


    }
    
}
extension ProfileVC:UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
        cell.selectionStyle = .none

       return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let objvc = self.storyboard?.instantiateViewController(withIdentifier: "JobDetailVC") as? JobDetailVC
//   self.navigationController?.pushViewController(objvc!, animated: true)

    }
}
