//
//  SideBaseViewController.swift
//  Employee Management
//
//  Created by vidya on 20/10/2020.
//

import UIKit

class SideBaseViewController: UIViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Set Left And Right Naviation Bar Items With Image
        setNavigationBarItem()
    }
}
