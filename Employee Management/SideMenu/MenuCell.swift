//
//  MenuCell.swift
//  Employee Management
//
//  Created by vidya on 20/10/2020.
//

import UIKit

class MenuCell: UITableViewCell {
    @IBOutlet weak var namelbl:UILabel!
    @IBOutlet weak var iconsImg:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
