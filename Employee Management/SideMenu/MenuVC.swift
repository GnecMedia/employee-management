//
//  MenuVC.swift
//  Employee Management
//
//  Created by vidya on 20/10/2020.
//

import UIKit
import SlideMenuControllerSwift
import RAMAnimatedTabBarController



class MenuVC: UIViewController {
    @IBOutlet weak var profileImg:UIImageView!
    @IBOutlet weak var namelbl:UILabel!
    @IBOutlet weak var emaillbl:UILabel!

   // @IBOutlet weak var menuTbl:UITableView!
    @IBOutlet var menuTbl: ExpandableTableView!
    var namelist = ["Dashboard","Mark Attendance","Leave","Holiday List","Bills","Report","Settings","About Us","Logout"]
    var imagelist = ["Group 57","nav_mark_attendance","Group 50","Group 54","bill","Group 58","Settings","Group 51","Group 46"]
    //Admin
    var adminlist = ["Dashboard","Announcement","Leave","Holiday","Employee","Report","Setings","About Us","Logout"]
    var adminImg = ["Group 57","nav_announcement","all_leave","nav_holiday","nav_employee","nav_report","nav_setting","nav_about_us","logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuTbl.expandableDelegate = self
        menuTbl.animation = .automatic
        menuTbl.register(UINib(nibName: "NormalCell", bundle: nil), forCellReuseIdentifier: NormalCell.ID)
        menuTbl.register(UINib(nibName: "ExpandedCell", bundle: nil), forCellReuseIdentifier: ExpandedCell.ID)
        menuTbl.register(UINib(nibName: "ExpandableCell", bundle: nil), forCellReuseIdentifier: ExpandableCell2.ID)
        self.profileImg.layer.cornerRadius = self.profileImg.frame.height / 2
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.bool(forKey: "is_Admin") == true{
         empolyeeApi()
        }else{
            if let full_name = UserDefaults.standard.object(forKey: "full_name") as? String{
                self.namelbl.text! = full_name

            }

            if let email = UserDefaults.standard.object(forKey: "email") as? String{
                self.emaillbl.text! = email
            }
            if let profile_picture = UserDefaults.standard.object(forKey: "profile_picture") as? String{
            self.profileImg.sd_setImage(with: URL(string: profile_picture))
            }
        }

    }
    func empolyeeApi(){
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        //let employee_id = UserDefaults.standard.object(forKey: "employee_id")

        let urlStr = baseUrl + get_employee_by_id
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"emp_id":userid!] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                let data = result["data"] as? [String:Any]
                let employee_data = data!["employee_data"] as! [String:Any]
                let email = employee_data["email"] as! String
                self.emaillbl.text! = email
                let name = employee_data["full_name"] as! String
                self.namelbl.text! = name
                let profile_picture = employee_data["profile_picture"] as! String
                self.profileImg.sd_setImage(with: URL(string: profile_picture))
            }
        }
    }
    //MARK:- Other Methods
    func changeMainViewController(to viewController: UIViewController) {
        //Change main viewcontroller of side menu view controller
        let navigationViewController = UINavigationController(rootViewController: viewController)
        slideMenuController()?.changeMainViewController(navigationViewController, close: true)
    }

    func logoutApi(){
        let userid = UserDefaults.standard.object(forKey: "user_id")
          let accesToken = UserDefaults.standard.object(forKey: "access_token")
           let urlStr = baseUrl + logout
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!] as [String:Any]
           postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
               print("Result--\(result)")
               if succ{
                UserDefaults.standard.setValue(false, forKey: "isLogin")

                let objvc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
                       self.navigationController?.pushViewController(objvc!, animated: true)
                self.changeMainViewController(to: objvc!)
               }
           }

    }

}

extension MenuVC: ExpandableDelegate {
    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCellsForRowAt indexPath: IndexPath) -> [UITableViewCell]? {
            if UserDefaults.standard.bool(forKey: "is_Admin") == true{

            if indexPath.row == 1{
            let cell1 = menuTbl.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
            cell1.titleLabel.text = "Create Announcement"
            cell1.imageIconImgView.image = UIImage(named: "nav_create_announcement")
            let cell2 = menuTbl.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
            cell2.titleLabel.text = "All Announcement"
            cell2.imageIconImgView.image = UIImage(named: "nav_all_announcement")
            return [cell1, cell2]
            }else if indexPath.row == 2{
                let cell3 = menuTbl.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
                cell3.titleLabel.text = "All Leave"
                cell3.imageIconImgView.image = UIImage(named: "nav_all_leave")
                return [cell3]
            }else if indexPath.row == 3{
                let cell1 = menuTbl.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
                cell1.titleLabel.text = "Add Holiday"
                cell1.imageIconImgView.image = UIImage(named: "nav_add_holiday")
                let cell2 = menuTbl.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
                cell2.titleLabel.text = "Holiday List"
                cell2.imageIconImgView.image = UIImage(named: "nav_holiday_list")
                return [cell1,cell2]
            }else if indexPath.row == 4{
                let cell1 = menuTbl.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
                cell1.titleLabel.text = "Add Employee"
                cell1.imageIconImgView.image = UIImage(named: "nav_add_employee")
                let cell2 = menuTbl.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
                cell2.titleLabel.text = "Employee List"
                cell2.imageIconImgView.image = UIImage(named: "nav_all_employee")
                return [cell1,cell2]

            }else if indexPath.row == 6{
                let cell2 = menuTbl.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
                cell2.titleLabel.text = "Profile"
                
                cell2.imageIconImgView.image = UIImage(named: "nav_profile")

                let cell3 = menuTbl.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
                cell3.titleLabel.text = "Change Password"
                cell3.imageIconImgView.image = UIImage(named: "nav_change_password")
                return [cell2, cell3]
                }

        }else{
            if indexPath.row == 2{
            let cell1 = menuTbl.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
            cell1.titleLabel.text = "Apple Leave"
            cell1.imageIconImgView.image = UIImage(named: "apple leave-1")
            let cell2 = menuTbl.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
            cell2.titleLabel.text = "Leave status"
            cell2.imageIconImgView.image = UIImage(named: "nav_all_leave")

            return [cell1, cell2]
            }else if indexPath.row == 4{
                let cell2 = menuTbl.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
                cell2.titleLabel.text = "Upload Bills"
                cell2.imageIconImgView.image = UIImage(named: "uplaod bill")

                let cell3 = menuTbl.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
                cell3.titleLabel.text = "Bill Status"
                cell3.imageIconImgView.image = UIImage(named: "bill status")

                return [cell2, cell3]
            

            }else if indexPath.row == 6{
                let cell2 = menuTbl.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
                cell2.titleLabel.text = "Profile"
                cell2.imageIconImgView.image = UIImage(named: "nav_profile")

                let cell3 = menuTbl.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
                cell3.titleLabel.text = "Change Password"
                cell3.imageIconImgView.image = UIImage(named: "Group 47")

                return [cell2, cell3]
                }

            }

    
        return nil
    
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightsForExpandedRowAt indexPath: IndexPath) -> [CGFloat]? {
        return [40]
    }
    
    func numberOfSections(in tableView: ExpandableTableView) -> Int {
        return 1
    }

    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int {
        if UserDefaults.standard.bool(forKey: "is_Admin") == true{
            return adminlist.count

        }else{
            return namelist.count

        }
    }

    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRow:\(indexPath.row)")
        if UserDefaults.standard.bool(forKey: "is_Admin") == true{

        if indexPath.row == 0{
            guard let tabViewController = storyboard?.instantiateViewController(withIdentifier:"CustomTabBarVC" ) as? CustomTabBarVC else {
                return
            }
            changeMainViewController(to: tabViewController)
        }else if indexPath.row == 5{
            guard let reportmenu = storyboard?.instantiateViewController(withIdentifier: "ReportVC") as? ReportVC  else {
                return
            }
            reportmenu.menuType = "menu"
            changeMainViewController(to: reportmenu)
        }else if indexPath.row == 7{
            guard let aboutvc = storyboard?.instantiateViewController(withIdentifier: "AboutUsVC") as? AboutUsVC  else {
                
                return
            }
            changeMainViewController(to: aboutvc)
        }else if indexPath.row == 8{
            print("logout")

            logoutApi()

        }
        }else{//User
            if indexPath.row == 0{

            guard let tabViewController = storyboard?.instantiateViewController(withIdentifier:"CustomTabBarVC" ) as? CustomTabBarVC else {
                return
            }
            changeMainViewController(to: tabViewController)
            }else if indexPath.row == 1{
                let objvc = self.storyboard?.instantiateViewController(withIdentifier: "MarkAttendanceVC") as? MarkAttendanceVC
                       self.navigationController?.pushViewController(objvc!, animated: true)
                    //objvc?.menuType = "menu"
                    changeMainViewController(to: objvc!)
            }else if indexPath.row == 3{
                let objvc = self.storyboard?.instantiateViewController(withIdentifier: "HolidayVC") as? HolidayVC
                       self.navigationController?.pushViewController(objvc!, animated: true)
                    objvc?.menuType = "menu"
                    changeMainViewController(to: objvc!)
            }else if indexPath.row == 5{
                let objvc = self.storyboard?.instantiateViewController(withIdentifier: "ReportVC") as? ReportVC
                       self.navigationController?.pushViewController(objvc!, animated: true)
                    objvc?.menuType = "menu"
                    changeMainViewController(to: objvc!)
            }else if indexPath.row == 7{
                let objvc = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsVC") as? AboutUsVC
                       self.navigationController?.pushViewController(objvc!, animated: true)
                   // objvc?.menuType = "menu"
                    changeMainViewController(to: objvc!)

            }else if indexPath.row == 8{
                print("logout")

                logoutApi()

            }
        
            
        }
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectExpandedRowAt indexPath: IndexPath) {
        print("didSelectExpandedRowAt:\(indexPath.row)")

//        let objvc = self.storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController
//   self.navigationController?.pushViewController(objvc!, animated: true)

    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCell: UITableViewCell, didSelectExpandedRowAt indexPath: IndexPath) {
        if let cell = expandedCell as? ExpandedCell {
            print("Tittle--\(cell.titleLabel.text ?? "")")
            if UserDefaults.standard.bool(forKey: "is_Admin") == true{
                if cell.titleLabel.text == "Create Announcement"{
                    let objvc = self.storyboard?.instantiateViewController(withIdentifier: "Admin_AddAnnounceVC") as? Admin_AddAnnounceVC
                           self.navigationController?.pushViewController(objvc!, animated: true)
                        objvc?.menuType = "menu"
                        changeMainViewController(to: objvc!)

                }else if cell.titleLabel.text == "All Announcement"{
                    let objvc = self.storyboard?.instantiateViewController(withIdentifier: "Admin_AllAnnouncementVC") as? Admin_AllAnnouncementVC
                           self.navigationController?.pushViewController(objvc!, animated: true)
                        objvc?.menuType = "menu"
                        changeMainViewController(to: objvc!)

                }else if cell.titleLabel.text == "All Leave"{
                    let objvc = self.storyboard?.instantiateViewController(withIdentifier: "AllLeaveVC") as? AllLeaveVC
                           self.navigationController?.pushViewController(objvc!, animated: true)
                        objvc?.menuType = "menu"
                        changeMainViewController(to: objvc!)

                }else if cell.titleLabel.text == "Add Holiday"{
                    let objvc = self.storyboard?.instantiateViewController(withIdentifier: "AddHolidayVC") as? AddHolidayVC
                           self.navigationController?.pushViewController(objvc!, animated: true)
                        objvc?.menuType = "menu"
                        changeMainViewController(to: objvc!)

                }else if cell.titleLabel.text == "Holiday List"{
                    let objvc = self.storyboard?.instantiateViewController(withIdentifier: "HolidayVC") as? HolidayVC
                           self.navigationController?.pushViewController(objvc!, animated: true)
                        objvc?.menuType = "menu"
                        changeMainViewController(to: objvc!)

                }else if cell.titleLabel.text == "Add Employee"{
                    let objvc = self.storyboard?.instantiateViewController(withIdentifier: "Admin_RegisterVC") as? Admin_RegisterVC
                           self.navigationController?.pushViewController(objvc!, animated: true)
                        objvc?.menuType = "menu"
                        changeMainViewController(to: objvc!)

                }else if cell.titleLabel.text == "Employee List"{
                    let objvc = self.storyboard?.instantiateViewController(withIdentifier: "AllEmployeeVC") as? AllEmployeeVC
                           self.navigationController?.pushViewController(objvc!, animated: true)
                        objvc?.menuType = "menu"
                        changeMainViewController(to: objvc!)

                }else if cell.titleLabel.text == "Profile"{
                    let objvc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as? ProfileVC
                           self.navigationController?.pushViewController(objvc!, animated: true)
                        objvc?.menuType = "menu"
                        changeMainViewController(to: objvc!)

                    
                }else if cell.titleLabel.text == "Change Password"{
                    let objvc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as? ChangePasswordVC
                           self.navigationController?.pushViewController(objvc!, animated: true)
                        objvc?.menuType = "menu"
                        changeMainViewController(to: objvc!)

                }

            }else{//user
                if cell.titleLabel.text == "Apple Leave"{
                    let objvc = self.storyboard?.instantiateViewController(withIdentifier: "ApplyLeaveVC") as? ApplyLeaveVC
                           self.navigationController?.pushViewController(objvc!, animated: true)
                        objvc?.menuType = "menu"
                        changeMainViewController(to: objvc!)
                }else if cell.titleLabel.text == "Leave status"{
                    let objvc = self.storyboard?.instantiateViewController(withIdentifier: "AllLeaveVC") as? AllLeaveVC
                           self.navigationController?.pushViewController(objvc!, animated: true)
                         objvc?.menuType = "menu"
                        changeMainViewController(to: objvc!)

                }else if cell.titleLabel.text == "Upload Bills"{
                    let objvc = self.storyboard?.instantiateViewController(withIdentifier: "UploadBillsVC") as? UploadBillsVC
                           self.navigationController?.pushViewController(objvc!, animated: true)
                         objvc?.menuType = "menu"
                        changeMainViewController(to: objvc!)

                }else if cell.titleLabel.text == "Bill Status"{
                    let objvc = self.storyboard?.instantiateViewController(withIdentifier: "AllBillsVC") as? AllBillsVC
                           self.navigationController?.pushViewController(objvc!, animated: true)
                         objvc?.menuType = "menu"
                        changeMainViewController(to: objvc!)
                }else if cell.titleLabel.text == "Profile"{
               
                    let objvc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as? ProfileVC
                           self.navigationController?.pushViewController(objvc!, animated: true)
                        objvc?.menuType = "menu"
                        changeMainViewController(to: objvc!)

                }else if cell.titleLabel.text == "Change Password"{
                    let objvc = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as? SettingVC
                           self.navigationController?.pushViewController(objvc!, animated: true)
                       // objvc?.menuType = "menu"
                        changeMainViewController(to: objvc!)

                }

            }
            
        }
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = expandableTableView.dequeueReusableCell(withIdentifier:NormalCell.ID) as! NormalCell
        if UserDefaults.standard.bool(forKey: "is_Admin") == true{
            cell.titleLabel.text = adminlist[indexPath.row]
            cell.iconImg.image = UIImage(named:adminImg[indexPath.row])

        }else{
        cell.titleLabel.text = namelist[indexPath.row]
        cell.iconImg.image = UIImage(named:imagelist[indexPath.row])
        
        }
        // guard let cell = expandableTableView.dequeueReusableCell(withIdentifier: parentCells[indexPath.section][indexPath.row]) else { return UITableViewCell() }
        return cell
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    @objc(expandableTableView:didCloseRowAt:) func expandableTableView(_ expandableTableView: UITableView, didCloseRowAt indexPath: IndexPath) {
        let cell = expandableTableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
        cell?.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
    }
    
    func expandableTableView(_ expandableTableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func expandableTableView(_ expandableTableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
}
}
