//
//  PageMenuVC.swift
//  Employee Management
//
//  Created by vidya on 02/11/2020.
//

import UIKit
import PageMenu

class PageMenuVC: UIViewController {
    var pageMenu : CAPSPageMenu?
    var startDate:String!
    var endDate:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "DETAIL REPORT"
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
            item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item

    // Do any additional setup after loading the view.
    }
    @objc func barButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        var controllerArray : [UIViewController] = []
        let attendence = self.storyboard?.instantiateViewController(withIdentifier: "AttendanceVC") as? AttendanceVC
        attendence?.startDateStr = startDate
        attendence?.endDateStr = endDate
        attendence!.title = "ATTENDENCE"
        controllerArray.append(attendence!)
        let leave = self.storyboard?.instantiateViewController(withIdentifier: "AllLeaveVC") as? AllLeaveVC
        leave!.title = "LEAVE"
        controllerArray.append(leave!)

        let holiday = self.storyboard?.instantiateViewController(withIdentifier: "HolidayVC") as? HolidayVC
        holiday!.title = "HOLIDAY"
        controllerArray.append(holiday!)
        let halfdayleave = self.storyboard?.instantiateViewController(withIdentifier: "HalfdayLeaveVC") as? HalfdayLeaveVC
        halfdayleave?.startDateStr = startDate
        halfdayleave?.endDateStr = endDate
        halfdayleave!.title = "HALFDAY LEAVE"
        controllerArray.append(halfdayleave!)
        let weekoff = self.storyboard?.instantiateViewController(withIdentifier: "WeekOffVC") as? WeekOffVC
        weekoff?.startDateStr = startDate
        weekoff?.endDateStr = endDate
        weekoff!.title = "WEEK OFF"
        controllerArray.append(weekoff!)

        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.clear),
            .viewBackgroundColor(UIColor.clear),
            .selectionIndicatorColor(UIColor.white),
            .bottomMenuHairlineColor(UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 80.0/255.0, alpha: 1.0)),
            .menuItemFont(UIFont(name: "poppins", size: 12.0)!),
            .menuHeight(40.0),
            .menuItemWidth(90.0),
            .centerMenuItems(true)
        ]
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y:80.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)

        self.addChild(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        
        pageMenu!.didMove(toParent: self)

        
    }
   
}
