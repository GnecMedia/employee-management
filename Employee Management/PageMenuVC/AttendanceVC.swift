//
//  AttendanceVC.swift
//  Employee Management
//
//  Created by vidya on 02/11/2020.
//

import UIKit

class AttendanceVC: UIViewController {
    @IBOutlet weak var attendanceTbl:UITableView!
    var attendance_dataList = [[String:Any]]()
    var startDateStr:String!
    var endDateStr:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Reachability.isConnectedToNetwork() == true{
        attendanceApiCall()
        }else{
            KAppDelegate.showAlertNotification(checkInternet)
        }

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

    }
    func attendanceApiCall(){
        self.attendance_dataList.removeAll()
        let userid = UserDefaults.standard.object(forKey: "user_id")
        let accesToken = UserDefaults.standard.object(forKey: "access_token")
        let urlStr = baseUrl + get_report_detail
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"start_date":startDateStr!,"end_date":endDateStr!] as [String:Any]
        postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
            print("Result--\(result)")
            if succ{
                let data = result["data"] as? [String:Any]
                let report_data = data!["report_data"] as! [String:Any]
                let attendance_data = report_data["attendance_data"] as! [[String:Any]]

                for dic in attendance_data{
                    self.attendance_dataList.append(dic)
                }
            }
            self.attendanceTbl.reloadData()
        }

    }
    
    
}
extension AttendanceVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.attendance_dataList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttendanceCell", for: indexPath) as! AttendanceCell
        cell.selectionStyle = .none
        let item = attendance_dataList[indexPath.row]
        if let logged_in_date = item["logged_in_date"] as? String{
            cell.datelbl.text! = logged_in_date
        }
        if let fullname = item["full_name"] as? String{
        cell.namelbl.text! = fullname
        }
        var loginTime:String! = ""
        var loginoutTime:String! = ""

        if let logged_in_time = item["logged_in_time"] as? String{
            loginTime = logged_in_time
        }
        if let logged_out_time = item["logged_out_time"] as? String{
            loginoutTime = logged_out_time

        }
        cell.timelbl.text! = loginTime + "-" + loginoutTime

       return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
}
