//
//  AllEmployeeCell.swift
//  Employee Management
//
//  Created by vidya on 09/11/2020.
//

import UIKit

class AllEmployeeCell: UITableViewCell {
    @IBOutlet weak var namelbl:UILabel!
    @IBOutlet weak var empidlbl:UILabel!
    @IBOutlet weak var designationlbl:UILabel!
    @IBOutlet weak var profileImg:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.profileImg.layer.cornerRadius = self.profileImg.frame.height / 2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
