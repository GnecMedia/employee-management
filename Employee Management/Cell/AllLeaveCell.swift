//
//  AllLeaveCell.swift
//  Employee Management
//
//  Created by vidya on 28/10/2020.
//

import UIKit

class AllLeaveCell: UITableViewCell {
    @IBOutlet weak var nameLbl:UILabel!
    @IBOutlet weak var dateLbl:UILabel!
    @IBOutlet weak var timelbl:UILabel!
    @IBOutlet weak var leaveTypeLbl:UILabel!
    @IBOutlet weak var remarkLbl:UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
