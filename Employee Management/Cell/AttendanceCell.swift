//
//  AttendanceCell.swift
//  Employee Management
//
//  Created by vidya on 02/11/2020.
//

import UIKit

class AttendanceCell: UITableViewCell {
    @IBOutlet weak var namelbl:UILabel!
    @IBOutlet weak var datelbl:UILabel!
    @IBOutlet weak var timelbl:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
