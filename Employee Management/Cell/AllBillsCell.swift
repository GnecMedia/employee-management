//
//  AllBillsCell.swift
//  Employee Management
//
//  Created by vidya on 06/11/2020.
//

import UIKit

class AllBillsCell: UITableViewCell {
    @IBOutlet weak var projectNameLbl:UILabel!
    @IBOutlet weak var statusLbl:UILabel!
    @IBOutlet weak var dateLbl:UILabel!
    @IBOutlet weak var billAmountLbl:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
