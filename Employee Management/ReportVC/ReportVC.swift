//
//  ReportVC.swift
//  Employee Management
//
//  Created by vidya on 28/10/2020.
//

import UIKit

class ReportVC: UIViewController {
    @IBOutlet weak var starDateLbl:UITextField!
    @IBOutlet weak var endDateLbl:UITextField!
    fileprivate var popViewController :  DateTimePopUp!
    var menuType:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Report"
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
            item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item

        // Do any additional setup after loading the view.
    }
    @objc func barButtonAction(){
        if self.menuType == "menu"{
            KAppDelegate.makeRootViewController()
        }else{
        self.navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func submitBtn(sender:UIButton){
        if self.starDateLbl.text!.isEmpty != true{
            if self.endDateLbl.text!.isEmpty != true{
                let objvc = self.storyboard?.instantiateViewController(withIdentifier: "ReportDataVC") as? ReportDataVC
                objvc?.startDateStr = self.starDateLbl.text!
                objvc?.endDateStr = self.endDateLbl.text!
                self.navigationController?.pushViewController(objvc!, animated: true)

            }else{
                KAppDelegate.showAlertNotification("Enter Start-Date")

            }
        }else{
            KAppDelegate.showAlertNotification("Enter Start-Date")
        }
        
    }
    @IBAction func startBtnAction(sender:UIButton){
        
        let bundle = Bundle(for: DateTimePopUp.self)
        self.popViewController = DateTimePopUp(nibName: "DateTimePopUp", bundle: bundle)
       self.popViewController.delegate = self
        self.popViewController.showInView(self.view,animated: true)

    }
    @IBAction func endeBtnAction(sender:UIButton){
        let bundle = Bundle(for: DateTimePopUp.self)
        self.popViewController = DateTimePopUp(nibName: "DateTimePopUp", bundle: bundle)
       self.popViewController.delegate = self
        self.popViewController.dateMode = "end_Date"
        self.popViewController.showInView(self.view,animated: true)

    }

}
extension ReportVC:DatePickerPopUpViewDelegate{
    func didPresDoneActionBtn(_ controller: DateTimePopUp, isDate: String) {
        self.starDateLbl.text! = isDate
    }
    
    func didPresDoneTimeActionBtn(_ controller: DateTimePopUp, isTime: String) {
        print("isTime")
    }
    
    func didPresEndDateActionBtn(_ controller: DateTimePopUp, isendDate: String) {
        self.endDateLbl.text! = isendDate

    }
    
    func didPresEndTimeActionBtn(_ controller: DateTimePopUp, isendTime: String) {
        print("isTime")

    }
    
    
}
