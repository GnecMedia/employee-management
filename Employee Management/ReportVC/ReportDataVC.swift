//
//  ReportDataVC.swift
//  Employee Management
//
//  Created by vidya on 02/11/2020.
//

import UIKit

class ReportDataVC: UIViewController {
    @IBOutlet weak var namelbl:UILabel!
    @IBOutlet weak var startdatelbl:UILabel!
    @IBOutlet weak var enddatelbl:UILabel!
    @IBOutlet weak var presentlbl:UILabel!
    @IBOutlet weak var total_full_day_absentlbl:UILabel!
    @IBOutlet weak var total_half_day_absentlbl:UILabel!
    @IBOutlet weak var total_week_offlbl:UILabel!
    @IBOutlet weak var total_holidaylbl:UILabel!
    @IBOutlet weak var salerylbl:UILabel!

    
    var startDateStr:String!
    var endDateStr:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Report"
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
            item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
        self.startdatelbl.text! = startDateStr!
        self.enddatelbl.text! = endDateStr!
        if Reachability.isConnectedToNetwork() == true{
        get_report_ApiCall()
        }else{
            KAppDelegate.showAlertNotification(checkInternet)
        }
        // Do any additional setup after loading the view.
    }
    @objc func barButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    func get_report_ApiCall(){
            let userid = UserDefaults.standard.object(forKey: "user_id")
            let accesToken = UserDefaults.standard.object(forKey: "access_token")
            let urlStr = baseUrl + get_report
        let leaveDetials = ["user_id":userid!,"access_token":accesToken!,"start_date":startDateStr!,"end_date":endDateStr!] as [String:Any]
            postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
                print("Result--\(result)")
                if succ{
                    let data = result["data"] as? [String:Any]
                    let report_data = data!["report_data"] as! Dictionary<String, Any>
                    print("report_data-\(report_data)")
                    let full_name = report_data["full_name"] as? String
                    self.namelbl.text! = "\(full_name!)"
                    let total_present = report_data["total_present"] as? Int
                    self.presentlbl.text! = "Total Present:-\(total_present!)"
                    let total_holiday = report_data["total_holiday"] as? Int
                    self.total_holidaylbl.text! = "Holiday:-\(total_holiday!)"
                    let salary = report_data["salary"] as? Int
                    self.salerylbl.text! = "Salary:-\(salary!)"
                    let total_half_day_absent = report_data["total_half_day_absent"] as? Int
                    self.total_half_day_absentlbl.text! = "Half Day:-\(total_half_day_absent!)"
                    let total_week_off = report_data["total_week_off"] as? Int
                    self.total_week_offlbl.text! = "Week Off:-\(total_week_off!)"
                    let total_full_day_absent = report_data["total_full_day_absent"] as? Int
                    self.total_full_day_absentlbl.text! = "Absent:-\(total_full_day_absent!)"

                }
            }
    }


    @IBAction func detailReport_BtnAction(sender:UIButton){
        let objvc = self.storyboard?.instantiateViewController(withIdentifier: "PageMenuVC") as? PageMenuVC
        objvc?.startDate = startDateStr!
        objvc?.endDate = endDateStr!
        
        self.navigationController?.pushViewController(objvc!, animated: true)

    }

}
