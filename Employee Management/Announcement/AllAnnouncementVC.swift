//
//  AllViAnnouncementVC.swift
//  Employee Management
//
//  Created by vidya on 28/10/2020.
//

import UIKit

class AllAnnouncementVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "All Announcement"
        let item = UIBarButtonItem(image: UIImage(named: "icons-dark-back"), style: .plain, target: self, action: #selector(barButtonAction))
            item.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
        get_annoncement_ApiCall()
        // Do any additional setup after loading the view.
    }
    @objc func barButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    func get_annoncement_ApiCall(){
            let userid = UserDefaults.standard.object(forKey: "user_id")
            let accesToken = UserDefaults.standard.object(forKey: "access_token")
            let urlStr = baseUrl + get_announcement
            let leaveDetials = ["user_id":userid!,"access_token":accesToken!] as [String:Any]
            postAPiwithOut(urlStr: urlStr, parameter: leaveDetials){(succ, result) in
                print("Result--\(result)")
                if succ{
                    let data = result["data"] as? [String:Any]
                    let annocement_data = data!["announcement_data"] as! [[String:Any]]
                    for dic in annocement_data{
                       // self.holidaylist.append(dic)
                    }
                }
             //   self.holidayTbl.reloadData()
            }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
